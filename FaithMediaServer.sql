-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (x86_64)
--
-- Host: localhost    Database: FaithMediaServer
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_checkin`
--

DROP TABLE IF EXISTS `account_checkin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_checkin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `details` longtext NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_checkin_6340c63c` (`user_id`),
  KEY `account_checkin_afbb987d` (`location_id`),
  CONSTRAINT `location_id_refs_id_1ecfeb79` FOREIGN KEY (`location_id`) REFERENCES `affiliation_location` (`id`),
  CONSTRAINT `user_id_refs_id_3d83684d` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_checkin`
--

LOCK TABLES `account_checkin` WRITE;
/*!40000 ALTER TABLE `account_checkin` DISABLE KEYS */;
INSERT INTO `account_checkin` VALUES (1,4,1,'Alone','2015-03-11 16:21:11');
/*!40000 ALTER TABLE `account_checkin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_device`
--

DROP TABLE IF EXISTS `account_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kind` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `uuid` varchar(100) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `decryption_key` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_device_owner_id_67d9741412b57b44_uniq` (`owner_id`,`uuid`),
  KEY `account_device_cb902d83` (`owner_id`),
  CONSTRAINT `owner_id_refs_id_10213809` FOREIGN KEY (`owner_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_device`
--

LOCK TABLES `account_device` WRITE;
/*!40000 ALTER TABLE `account_device` DISABLE KEYS */;
INSERT INTO `account_device` VALUES (1,2,'Test iPhone','abcd',8,'^Dm@m-75ob=-Gsgc'),(2,2,'Same iPhone','abcd',4,'wHv_6h--aH_Pzoc');
/*!40000 ALTER TABLE `account_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_downloadtoken`
--

DROP TABLE IF EXISTS `account_downloadtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_downloadtoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) NOT NULL,
  `path` varchar(254) NOT NULL,
  `valid` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_downloadtoken`
--

LOCK TABLES `account_downloadtoken` WRITE;
/*!40000 ALTER TABLE `account_downloadtoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_downloadtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_user`
--

DROP TABLE IF EXISTS `account_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  `security_question` int(11) NOT NULL,
  `security_answer` varchar(255) NOT NULL,
  `credit` decimal(6,2) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `church_id` int(11) NOT NULL,
  `subscribed` tinyint(1) NOT NULL,
  `title` varchar(20) NOT NULL,
  `status` varchar(30) NOT NULL,
  `decryption_key` varchar(20) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `family_group_id` int(11),
  `birth_date` date,
  `sex` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `account_user_60d6ec24` (`church_id`),
  KEY `account_user_ee8a4419` (`family_group_id`),
  CONSTRAINT `church_id_refs_id_609a93ab` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`),
  CONSTRAINT `family_group_id_refs_id_40434c58` FOREIGN KEY (`family_group_id`) REFERENCES `affiliation_familygroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_user`
--

LOCK TABLES `account_user` WRITE;
/*!40000 ALTER TABLE `account_user` DISABLE KEYS */;
INSERT INTO `account_user` VALUES (1,'pbkdf2_sha256$12000$354pltGrYcKH$GiEEtJMk5+uQwoXKRXy1eH0bE1p4tEXW+lSMC7zH+i4=','2015-12-11 23:58:26',1,'super@test.com','Super','Admin',1,1,'2015-02-24 01:02:00',0,'Test',0.00,'Test','Test','Test','Test','',1,0,'Mr.','Single','r%@Thso@t^zuWnb','',NULL,NULL,''),(2,'pbkdf2_sha256$12000$uDE0q7N0UYYQ$2FjhSqvhYN3K8AJEoE/PuIDD41Q3d/FNJO6pue5+aTQ=','2015-06-04 20:53:17',0,'admin01@test.com','Admin','Church 01',1,1,'2015-02-25 20:47:00',0,'Test',0.00,'Test','Test','Test','Test','',2,0,'Mr.','Single','41ot5E-:tzdhj','',NULL,NULL,''),(3,'pbkdf2_sha256$12000$iyNGfAF5v0u8$ZV4g4nwNbw8QeJ6QVhRAqpDUBAtO/MWgzeiSTqzb+xI=','2015-03-02 23:22:35',0,'admin02@test.com','Admin','Church 02',1,1,'2015-02-25 20:48:00',0,'Test',0.00,'Test','Test','Test','Test','',3,0,'Mr.','Single','V9=he20fiVf-^C','',NULL,NULL,''),(4,'pbkdf2_sha256$12000$DQQR3uuonkqJ$iIODMtC00ZJRH8zRtqBeDdKQRoh3ParfIfQQR56BiZ8=','2015-05-05 00:06:50',0,'user11@test.com','Church 1','User 1',0,1,'2015-02-25 20:52:00',0,'Test',0.00,'Test','Test','Test','Test','',2,0,'Mr.','Single','-JF42J_Q/^vG3m','',1,NULL,''),(5,'pbkdf2_sha256$12000$vz4fcg2EBxyo$TwUPezQeMC7nWd3httgNw6oje93LM+i6bUDAuvCew8g=','2015-02-26 04:09:00',0,'user12@test.com','Church 1','User 2',0,1,'2015-02-25 20:53:00',0,'Test',0.00,'Test','Test','Test','Test','',2,0,'Mr.','Single','-LN+UROAa:pLG','',1,NULL,''),(6,'pbkdf2_sha256$12000$9Ev130qQ9uyF$oxycO6bU75SuasV6e2++YMDRDrmpxTZA8uBK/uIcY4E=','2015-02-26 04:19:00',0,'user21@test.com','Church 2','User 1',0,1,'2015-02-25 20:53:00',0,'Test',0.00,'Test','Test','Test','Test','',3,0,'Mr.','Single','g-%eGXpE+%N_Jjh','',2,NULL,''),(7,'pbkdf2_sha256$12000$hXaFloutZAXx$d4MxZKQjTxrQrpNShVe7A6/J/TJILKvTV1hugvdbZGs=','2015-02-26 04:22:00',0,'user22@test.com','Church 2','User 2',0,1,'2015-02-25 20:54:00',0,'Test',0.00,'Test','Test','Test','Test','',3,0,'Mr.','Single','pJK7G%t-jR^D/R','',3,NULL,''),(8,'pbkdf2_sha256$12000$hCCKZ4AoFD5g$NF8p499DkCuuKM0KEaYwrvDhSV5cundFob9ofYTiWcc=','2015-07-27 15:06:36',0,'user01@test.com','No Church','User 1',0,1,'2015-02-25 20:55:00',0,'Test',0.00,'Test','Test','Test','Test','',1,0,'Mr.','Single','+eC@dy^b%kkhHg','',NULL,NULL,''),(9,'pbkdf2_sha256$12000$JIDHooplqVob$9UNPa76xDf24cjKMyv9yY0vSoDPRv0c2xwUf4NY37u4=','2015-05-26 15:03:24',0,'admin@test.com','Faith','Admin',1,1,'2015-03-11 00:00:00',0,'test',0.00,'Test','Test','Test','Test','',1,0,'Mr.','Single','-=nsbEwPh8WF-e','',NULL,NULL,''),(10,'pbkdf2_sha256$12000$9F8x7VAV1axm$Mc5XeDQ/TbPa1BiL/zwHOICiWp7tBllp/eKVbTqWV6A=','2015-03-11 21:31:53',0,'service@test.com','Customer','Service',1,1,'2015-03-11 00:01:00',0,'test',0.00,'Test','Test','Test','Test','',1,0,'Mr.','Single','::+B:Xpxm^XdOf@R','',NULL,NULL,''),(12,'pbkdf2_sha256$12000$4ics6eHKQN94$HsmQW1DGByE1WnJF2UvHNhpxBHr5uNiSWz1E/y85dQo=','2015-07-27 15:00:00',0,'yamanqui@gmail.com','Yamanqui','García Rosales',0,1,'2015-07-27 15:00:00',0,'yamis',0.00,'México','Nuevo Leon','Monterrey','Río Nilo 229 A, Col. Roma','64700',1,0,'Mr.','Married','+l_g%sSHHN^P+p','(81) 8359 5395',NULL,'1980-11-14','male');
/*!40000 ALTER TABLE `account_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_user_groups`
--

DROP TABLE IF EXISTS `account_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_user_groups_user_id_229249dfa04cbbd2_uniq` (`user_id`,`group_id`),
  KEY `account_user_groups_6340c63c` (`user_id`),
  KEY `account_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `group_id_refs_id_de83ef11` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_28c47504` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_user_groups`
--

LOCK TABLES `account_user_groups` WRITE;
/*!40000 ALTER TABLE `account_user_groups` DISABLE KEYS */;
INSERT INTO `account_user_groups` VALUES (9,1,1),(42,2,3),(11,3,3),(50,4,4),(18,5,4),(19,6,4),(21,7,4),(49,8,4),(23,9,1),(25,10,2),(56,12,4);
/*!40000 ALTER TABLE `account_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_user_user_permissions`
--

DROP TABLE IF EXISTS `account_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_user_user_permissions_user_id_533d6328df5d4e69_uniq` (`user_id`,`permission_id`),
  KEY `account_user_user_permissions_6340c63c` (`user_id`),
  KEY `account_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_7a4f8593` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_d5922e8c` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_user_user_permissions`
--

LOCK TABLES `account_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `account_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_church`
--

DROP TABLE IF EXISTS `affiliation_church`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_church` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `denomination` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `pastor` varchar(200) NOT NULL,
  `logo` longtext NOT NULL,
  `graphic` longtext NOT NULL,
  `background_color` varchar(7) NOT NULL,
  `background_image` longtext NOT NULL,
  `background_image_mode` int(11) NOT NULL,
  `welcome_message` longtext NOT NULL,
  `pastor_message` longtext NOT NULL,
  `video` longtext NOT NULL,
  `use_video` tinyint(1) NOT NULL,
  `aka` varchar(100) NOT NULL,
  `services_sunday` varchar(300),
  `services_monday` varchar(300),
  `services_tuesday` varchar(300),
  `services_wednesday` varchar(300),
  `services_thursday` varchar(300),
  `services_friday` varchar(300),
  `services_saturday` varchar(300),
  `signed_in` date NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `account_id` varchar(10) NOT NULL,
  `sub_id` varchar(10) NOT NULL,
  `merchant_pin` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_church`
--

LOCK TABLES `affiliation_church` WRITE;
/*!40000 ALTER TABLE `affiliation_church` DISABLE KEYS */;
INSERT INTO `affiliation_church` VALUES (1,'Non-Affiliated','None','None','None','None','None','None','','','None','','','','',0,'','','',0,'Non-Affiliated','','','','','','','','2014-03-15','America/Chicago','MPNAB','FMACT','1234567890'),(2,'Test Church 01','Testing','','Mexico','Jalisc','Guadala','1231 aslf','124','','Nadie','','','','',0,'','','',0,'','08:35:00,12:20:00','19:03:00','19:03:00','19:03:00','19:03:00','18:25:00,11:00:00','08:00:00,09:00:00,10:00:00,11:00:00,12:00:00,13:00:00','2015-02-26','America/Monterrey','MPNAB','FMACT','1234567890'),(3,'Test Church 02','Testing','','USA','Fake','Fake','123 fake','','','áéíóúñ','','','','',0,'','','',0,'','','','','','','','','2015-02-26','US/Central','MPNAB','FMACT','1234567890'),(4,'Empty','empty','','empty','emtpy','emtpy','emtpy','','','emtpy','','','','',0,'','','',0,'','','','','','','','','2015-02-26','US/Central','MPNAB','FMACT','1234567890');
/*!40000 ALTER TABLE `affiliation_church` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_church_editors`
--

DROP TABLE IF EXISTS `affiliation_church_editors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_church_editors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliation_church_editors_church_id_7d9025808d08ca2d_uniq` (`church_id`,`user_id`),
  KEY `affiliation_church_editors_60d6ec24` (`church_id`),
  KEY `affiliation_church_editors_6340c63c` (`user_id`),
  CONSTRAINT `church_id_refs_id_8d4a9055` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`),
  CONSTRAINT `user_id_refs_id_8d267b26` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_church_editors`
--

LOCK TABLES `affiliation_church_editors` WRITE;
/*!40000 ALTER TABLE `affiliation_church_editors` DISABLE KEYS */;
INSERT INTO `affiliation_church_editors` VALUES (31,2,2),(34,3,3);
/*!40000 ALTER TABLE `affiliation_church_editors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_churchreceipt`
--

DROP TABLE IF EXISTS `affiliation_churchreceipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_churchreceipt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `church_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `item_content_type_id` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `donation` decimal(6,2),
  `sale` decimal(6,2),
  `tax` decimal(6,2),
  `item_kind` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliation_churchreceipt_60d6ec24` (`church_id`),
  KEY `affiliation_churchreceipt_6340c63c` (`user_id`),
  KEY `affiliation_churchreceipt_1a9f7c96` (`item_content_type_id`),
  CONSTRAINT `church_id_refs_id_cd9736d0` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`),
  CONSTRAINT `item_content_type_id_refs_id_516dad2e` FOREIGN KEY (`item_content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_66ab1424` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_churchreceipt`
--

LOCK TABLES `affiliation_churchreceipt` WRITE;
/*!40000 ALTER TABLE `affiliation_churchreceipt` DISABLE KEYS */;
INSERT INTO `affiliation_churchreceipt` VALUES (1,'2015-02-26 02:07:04',1,1,1,15,'Book - Test Book',NULL,341.12,28.14,1),(2,'2015-02-26 04:02:17',2,8,1,24,'Giving: Giving 01',34.35,NULL,NULL,7),(3,'2015-02-26 04:02:17',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(4,'2015-02-26 04:06:34',2,2,1,22,'Thu, 26 Feb 2015 - Event 01',14.75,NULL,NULL,6),(5,'2015-02-26 04:06:34',2,2,1,24,'Giving: Giving 01',83.12,NULL,NULL,7),(6,'2015-02-26 04:06:34',2,2,4,24,'Tithing: Tithing 02',83.32,NULL,NULL,8),(7,'2015-02-26 04:09:29',2,4,2,22,'Thu, 26 Feb 2015 - Event 02',3.00,NULL,NULL,6),(8,'2015-02-26 04:09:29',2,4,3,24,'Tithing: Tithing 01',3.19,NULL,NULL,8),(9,'2015-02-26 04:09:29',2,4,4,24,'Tithing: Tithing 02',43.23,NULL,NULL,8),(10,'2015-02-26 04:11:37',2,5,1,15,'Book - Test Book',NULL,341.12,28.14,1),(11,'2015-02-26 04:11:37',2,5,2,22,'Thu, 26 Feb 2015 - Event 02',8.17,NULL,NULL,6),(12,'2015-02-26 04:11:37',2,5,2,24,'Giving: Giving 02',79.34,NULL,NULL,7),(13,'2015-02-26 04:18:52',3,3,1,15,'Book - Test Book',NULL,341.12,28.14,1),(14,'2015-02-26 04:18:52',3,3,5,24,'Giving: Giving 01',23.56,NULL,NULL,7),(15,'2015-02-26 04:18:52',3,3,8,24,'Tithing: Tithing 02',123.24,NULL,NULL,8),(16,'2015-02-26 04:22:15',3,6,3,22,'Thu, 26 Feb 2015 - Event 01',82.14,NULL,NULL,6),(17,'2015-02-26 04:22:15',3,6,4,22,'Thu, 26 Feb 2015 - Event 02',5.23,NULL,NULL,6),(18,'2015-02-26 04:22:15',3,6,8,24,'Tithing: Tithing 02',13.52,NULL,NULL,8),(19,'2015-02-26 04:23:57',3,7,7,24,'Tithing: Tithing 01',381.12,NULL,NULL,8),(20,'2015-02-26 04:23:57',3,7,8,24,'Tithing: Tithing 02',13.12,NULL,NULL,8),(21,'2015-02-26 04:23:57',3,7,7,24,'Tithing: Tithing 01',93.42,NULL,NULL,8),(22,'2015-07-27 13:59:52',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(23,'2015-07-27 14:01:56',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(24,'2015-07-27 14:49:57',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(25,'2015-07-27 14:52:07',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(26,'2015-07-27 14:56:47',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(27,'2015-07-27 15:02:32',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(28,'2015-07-27 15:05:22',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(29,'2015-07-27 15:07:21',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1),(30,'2015-07-27 15:10:12',1,8,1,15,'Book - Test Book',NULL,341.12,28.14,1);
/*!40000 ALTER TABLE `affiliation_churchreceipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_event`
--

DROP TABLE IF EXISTS `affiliation_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `church_id` int(11),
  `cost` decimal(6,2) NOT NULL,
  `group_id` int(11),
  `location_address` varchar(200) NOT NULL,
  `location_city` varchar(200) NOT NULL,
  `location_state` varchar(100) NOT NULL,
  `location_zip` varchar(20) NOT NULL,
  `location_name` varchar(200) NOT NULL,
  `time` varchar(300) NOT NULL,
  `age_range` varchar(100) NOT NULL,
  `organizer_name` varchar(100) NOT NULL,
  `organizer_phone` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliation_event_60d6ec24` (`church_id`),
  KEY `affiliation_event_5f412f9a` (`group_id`),
  CONSTRAINT `church_id_refs_id_43ece384` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`),
  CONSTRAINT `group_id_refs_id_e1b65d6f` FOREIGN KEY (`group_id`) REFERENCES `affiliation_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_event`
--

LOCK TABLES `affiliation_event` WRITE;
/*!40000 ALTER TABLE `affiliation_event` DISABLE KEYS */;
INSERT INTO `affiliation_event` VALUES (1,'Event 01','Event 01',2,14.75,NULL,'','','','','','DTSTART:20150225T142200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=22;BYSECOND=0','','',''),(2,'Event 02','Event 02',2,8.17,NULL,'','','','','','DTSTART:20150225T142200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=22;BYSECOND=0','','',''),(3,'Event 01','Event 01',3,82.14,NULL,'','','','','','DTSTART:20150325T194500+0530 RRULE:FREQ=DAILY;BYHOUR=19;BYMINUTE=45;BYSECOND=0','','',''),(4,'Event 02','Event 02',3,5.23,NULL,'','','','','','DTSTART:20150225T144200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=42;BYSECOND=0','','','');
/*!40000 ALTER TABLE `affiliation_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_event_registered`
--

DROP TABLE IF EXISTS `affiliation_event_registered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_event_registered` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliation_event_registered_event_id_2491d794b94c88f8_uniq` (`event_id`,`user_id`),
  KEY `affiliation_event_registered_a41e20fe` (`event_id`),
  KEY `affiliation_event_registered_6340c63c` (`user_id`),
  CONSTRAINT `event_id_refs_id_702640d5` FOREIGN KEY (`event_id`) REFERENCES `affiliation_event` (`id`),
  CONSTRAINT `user_id_refs_id_89f975c6` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_event_registered`
--

LOCK TABLES `affiliation_event_registered` WRITE;
/*!40000 ALTER TABLE `affiliation_event_registered` DISABLE KEYS */;
INSERT INTO `affiliation_event_registered` VALUES (11,1,2),(12,2,4),(13,2,5),(14,3,6),(10,4,6);
/*!40000 ALTER TABLE `affiliation_event_registered` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_familygroup`
--

DROP TABLE IF EXISTS `affiliation_familygroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_familygroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_id` int(11) NOT NULL,
  `family_name` varchar(100) NOT NULL,
  `husband_name` varchar(100) NOT NULL,
  `wife_name` varchar(100) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliation_familygroup_60d6ec24` (`church_id`),
  CONSTRAINT `church_id_refs_id_da5a97af` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_familygroup`
--

LOCK TABLES `affiliation_familygroup` WRITE;
/*!40000 ALTER TABLE `affiliation_familygroup` DISABLE KEYS */;
INSERT INTO `affiliation_familygroup` VALUES (1,2,'01 Family','Him','Her','Test','Test','Test','Test','',''),(2,3,'02 Family 1','user 21','None','Test','test','test','test','',''),(3,3,'02 Family 2','user 22','None','test','test','test','test','','');
/*!40000 ALTER TABLE `affiliation_familygroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_giving`
--

DROP TABLE IF EXISTS `affiliation_giving`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_giving` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `church_id` int(11) NOT NULL,
  `kind` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliation_giving_60d6ec24` (`church_id`),
  CONSTRAINT `church_id_refs_id_6527c7f9` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_giving`
--

LOCK TABLES `affiliation_giving` WRITE;
/*!40000 ALTER TABLE `affiliation_giving` DISABLE KEYS */;
INSERT INTO `affiliation_giving` VALUES (1,'Giving 01','Giving 01',2,'giving'),(2,'Giving 02','Giving 02',2,'giving'),(3,'Tithing 01','Tithing 01',2,'tithing'),(4,'Tithing 02','Tighing 02',2,'tithing'),(5,'Giving 01','Giving 01',3,'giving'),(6,'Giving 02','Giving 02',3,'giving'),(7,'Tithing 01','Tithing 01',3,'tithing'),(8,'Tithing 02','Tithing 02',3,'tithing');
/*!40000 ALTER TABLE `affiliation_giving` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_group`
--

DROP TABLE IF EXISTS `affiliation_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `church_id` int(11) NOT NULL,
  `purpose` longtext NOT NULL,
  `age_range` varchar(100) NOT NULL,
  `organizer_name` varchar(100) NOT NULL,
  `organizer_phone` varchar(50) NOT NULL,
  `kind` varchar(10) NOT NULL,
  `location_address` varchar(200) NOT NULL,
  `location_city` varchar(200) NOT NULL,
  `location_state` varchar(100) NOT NULL,
  `location_zip` varchar(20) NOT NULL,
  `location_name` varchar(200) NOT NULL,
  `meeting` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliation_group_60d6ec24` (`church_id`),
  CONSTRAINT `church_id_refs_id_18297104` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_group`
--

LOCK TABLES `affiliation_group` WRITE;
/*!40000 ALTER TABLE `affiliation_group` DISABLE KEYS */;
INSERT INTO `affiliation_group` VALUES (1,'Test Group 11','Test Group',2,'','','','','group','','','','','','DTSTART:20150301T204100-0600 RRULE:FREQ=DAILY;BYHOUR=20;BYMINUTE=41;BYSECOND=0'),(2,'Test Group 12','Another Group',2,'','','','','group','','','','','Here',''),(3,'Group 21','Group 21',3,'','','','','group','','','','','Somewhere','DTSTART:20150317T120000-0500 RRULE:FREQ=MONTHLY;UNTIL=20151217T120000-0600;COUNT=12;BYHOUR=12;BYMINUTE=0;BYSECOND=0'),(4,'Group 22','Group 22',3,'','','','','group','','','','','over the rainbow','DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=MO,WE,FR;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0');
/*!40000 ALTER TABLE `affiliation_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_group_admins`
--

DROP TABLE IF EXISTS `affiliation_group_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_group_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliation_group_admins_group_id_54f90224cd8ed64e_uniq` (`group_id`,`user_id`),
  KEY `affiliation_group_admins_5f412f9a` (`group_id`),
  KEY `affiliation_group_admins_6340c63c` (`user_id`),
  CONSTRAINT `group_id_refs_id_1371d94a` FOREIGN KEY (`group_id`) REFERENCES `affiliation_group` (`id`),
  CONSTRAINT `user_id_refs_id_83a6e11c` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_group_admins`
--

LOCK TABLES `affiliation_group_admins` WRITE;
/*!40000 ALTER TABLE `affiliation_group_admins` DISABLE KEYS */;
INSERT INTO `affiliation_group_admins` VALUES (4,3,6);
/*!40000 ALTER TABLE `affiliation_group_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_group_members`
--

DROP TABLE IF EXISTS `affiliation_group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_group_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliation_group_members_group_id_35d5d5fb14a4c150_uniq` (`group_id`,`user_id`),
  KEY `affiliation_group_members_5f412f9a` (`group_id`),
  KEY `affiliation_group_members_6340c63c` (`user_id`),
  CONSTRAINT `group_id_refs_id_e5a44a6a` FOREIGN KEY (`group_id`) REFERENCES `affiliation_group` (`id`),
  CONSTRAINT `user_id_refs_id_592a0482` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_group_members`
--

LOCK TABLES `affiliation_group_members` WRITE;
/*!40000 ALTER TABLE `affiliation_group_members` DISABLE KEYS */;
INSERT INTO `affiliation_group_members` VALUES (7,1,2),(8,1,4),(9,2,4),(10,2,5),(13,3,6);
/*!40000 ALTER TABLE `affiliation_group_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_location`
--

DROP TABLE IF EXISTS `affiliation_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `church_id` int(11) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliation_location_name_7987ae7d118ce14d_uniq` (`name`,`church_id`),
  KEY `affiliation_location_60d6ec24` (`church_id`),
  CONSTRAINT `church_id_refs_id_640063ed` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_location`
--

LOCK TABLES `affiliation_location` WRITE;
/*!40000 ALTER TABLE `affiliation_location` DISABLE KEYS */;
INSERT INTO `affiliation_location` VALUES (1,'Main Hall',2,'Test','Test','Test','Test','');
/*!40000 ALTER TABLE `affiliation_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_news`
--

DROP TABLE IF EXISTS `affiliation_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `time` datetime NOT NULL,
  `church_id` int(11),
  `group_id` int(11),
  `kind` varchar(15) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliation_news_60d6ec24` (`church_id`),
  KEY `affiliation_news_5f412f9a` (`group_id`),
  CONSTRAINT `church_id_refs_id_9ca9f5dc` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`),
  CONSTRAINT `group_id_refs_id_f01c6f65` FOREIGN KEY (`group_id`) REFERENCES `affiliation_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_news`
--

LOCK TABLES `affiliation_news` WRITE;
/*!40000 ALTER TABLE `affiliation_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliation_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `affiliation_suggestedchurch`
--

DROP TABLE IF EXISTS `affiliation_suggestedchurch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `affiliation_suggestedchurch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `corp_name` varchar(100) NOT NULL,
  `denomination` varchar(100) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `pastor` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `affiliation_suggestedchurch`
--

LOCK TABLES `affiliation_suggestedchurch` WRITE;
/*!40000 ALTER TABLE `affiliation_suggestedchurch` DISABLE KEYS */;
/*!40000 ALTER TABLE `affiliation_suggestedchurch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry`
--

DROP TABLE IF EXISTS `archive_logentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initiator_id` int(11),
  `initiator_name` varchar(255) NOT NULL,
  `action` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `action_time` datetime NOT NULL,
  `user_names` longtext NOT NULL,
  `church_names` longtext NOT NULL,
  `book_names` longtext NOT NULL,
  `charge` decimal(6,2),
  `movie_names` longtext NOT NULL,
  `song_names` longtext NOT NULL,
  `sermon_names` longtext NOT NULL,
  `gift_names` longtext NOT NULL,
  `group_names` longtext NOT NULL,
  `news_names` longtext NOT NULL,
  `giving_names` longtext NOT NULL,
  `location_names` longtext NOT NULL,
  `family_group_names` longtext NOT NULL,
  `event_names` longtext NOT NULL,
  `object_content_type_id` int(11) NOT NULL,
  `object_id` int(10) unsigned,
  `object_name` varchar(200) NOT NULL,
  `account_number` longtext,
  `auth_code` longtext,
  `auth_number` longtext,
  `order_id` longtext,
  `status` longtext,
  `pay_type` longtext,
  `trans_id` longtext,
  `entry_method` longtext,
  `ref_code` longtext,
  `sub_total` decimal(6,2),
  `tax` decimal(6,2),
  `json_cart` longtext,
  PRIMARY KEY (`id`),
  KEY `archive_logentry_82fcc517` (`initiator_id`),
  KEY `archive_logentry_b5399038` (`object_content_type_id`),
  CONSTRAINT `initiator_id_refs_id_58485fac` FOREIGN KEY (`initiator_id`) REFERENCES `account_user` (`id`),
  CONSTRAINT `object_content_type_id_refs_id_5e75db0a` FOREIGN KEY (`object_content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry`
--

LOCK TABLES `archive_logentry` WRITE;
/*!40000 ALTER TABLE `archive_logentry` DISABLE KEYS */;
INSERT INTO `archive_logentry` VALUES (1,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\npastor\'s name - from \"Tester\" to \"áéíóúñ\"\neditors - [Admin Church 02 (admin02@test.com)]','2015-02-26 06:42:25','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'Super Admin',0,'Church \"Empty, emtpy, empty\" Created:\nID - \"4\"\nname - \"Empty\"\nalso known as - \"\"\ndenomination - \"empty\"\ncontact name - \"\"\ncountry - \"empty\"\nstate - \"emtpy\"\ncity - \"emtpy\"\naddress - \"emtpy\"\nzip code - \"\"\nphone number - \"\"\npastor\'s name - \"emtpy\"\nsign in date - \"2015-02-26\"\nSunday service hours - \"[]\"\nMonday service hours - \"[]\"\nTuesday service hours - \"[]\"\nWednesday service hours - \"[]\"\nThursday service hours - \"[]\"\nFriday service hours - \"[]\"\nSaturday service hours - \"[]\"\nchurch\'s logo - \"\"\nmain graphic - \"\"\nmain video - \"\"\nuse video - \"False\"\nbackground color - \"\"\nbackground image - \"\"\nbackground image mode - \"0\"\nwelcome message - \"\"\npastor\'s message - \"\"\neditors - []','2015-02-26 23:33:20','','Empty, emtpy, empty','',NULL,'','','','','','','','','','',20,4,'Empty, emtpy, empty',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,'Super Admin',0,'Family \"Him and Her 01 Family\" Created:\nID - \"1\"\nchurch - \"Test Church 01, Guadala, Mexico\"\nfamily last name - \"01 Family\"\nhusband first name - \"Him\"\nwife first name - \"Her\"\ncountry - \"Test\"\nstate - \"Test\"\ncity - \"Test\"\naddress - \"Test\"\nzip code - \"\"','2015-02-27 21:07:24','','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','Him and Her 01 Family','',26,1,'Him and Her 01 Family',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-02-27 21:07:24','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,'Super Admin',1,'User \"Church 1 User 1 (user11@test.com)\" Changed:\nlast login - from \"2015-02-26 04:06:58+00:00\" to \"2015-02-25 22:06:00-06:00\"\nfamily group - from \"None\" to \"Him and Her 01 Family\"\ngroups - [Consumer]\nuser permissions - []','2015-02-27 21:08:01','Church 1 User 1 (user11@test.com)','','',NULL,'','','','','','','','','Him and Her 01 Family','',34,4,'Church 1 User 1 (user11@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,1,'Super Admin',1,'User \"Church 1 User 2 (user12@test.com)\" Changed:\nlast login - from \"2015-02-26 04:09:45+00:00\" to \"2015-02-25 22:09:00-06:00\"\nfamily group - from \"None\" to \"Him and Her 01 Family\"\ngroups - [Consumer]\nuser permissions - []','2015-02-27 21:08:10','Church 1 User 2 (user12@test.com)','','',NULL,'','','','','','','','','Him and Her 01 Family','',34,5,'Church 1 User 2 (user12@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,1,'Super Admin',0,'Family \"user 21 and None 02 Family 1\" Created:\nID - \"2\"\nchurch - \"Test Church 02, Fake, USA\"\nfamily last name - \"02 Family 1\"\nhusband first name - \"user 21\"\nwife first name - \"None\"\ncountry - \"Test\"\nstate - \"test\"\ncity - \"test\"\naddress - \"test\"\nzip code - \"\"','2015-02-27 21:09:06','','Test Church 02, Fake, USA','',NULL,'','','','','','','','','user 21 and None 02 Family 1','',26,2,'user 21 and None 02 Family 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,1,'Super Admin',0,'Family \"user 22 and None 02 Family 2\" Created:\nID - \"3\"\nchurch - \"Test Church 02, Fake, USA\"\nfamily last name - \"02 Family 2\"\nhusband first name - \"user 22\"\nwife first name - \"None\"\ncountry - \"test\"\nstate - \"test\"\ncity - \"test\"\naddress - \"test\"\nzip code - \"\"','2015-02-27 21:09:06','','Test Church 02, Fake, USA','',NULL,'','','','','','','','','user 22 and None 02 Family 2','',26,3,'user 22 and None 02 Family 2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-02-27 21:09:06','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,1,'Super Admin',1,'User \"Church 2 User 1 (user21@test.com)\" Changed:\nlast login - from \"2015-02-26 04:19:37+00:00\" to \"2015-02-25 22:19:00-06:00\"\nfamily group - from \"None\" to \"user 21 and None 02 Family 1\"\ngroups - [Consumer]\nuser permissions - []','2015-02-27 21:09:33','Church 2 User 1 (user21@test.com)','','',NULL,'','','','','','','','','user 21 and None 02 Family 1','',34,6,'Church 2 User 1 (user21@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,1,'Super Admin',1,'User \"Church 2 User 2 (user22@test.com)\" Changed:\nlast login - from \"2015-02-26 04:22:26+00:00\" to \"2015-02-25 22:22:00-06:00\"\nfamily group - from \"None\" to \"user 22 and None 02 Family 2\"\ngroups - [Consumer]\nuser permissions - []','2015-02-27 21:09:44','Church 2 User 2 (user22@test.com)','','',NULL,'','','','','','','','','user 22 and None 02 Family 2','',34,7,'Church 2 User 2 (user22@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,1,'Super Admin',1,'User \"Church 2 User 2 (user22@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-02-27 21:09:51','Church 2 User 2 (user22@test.com)','','',NULL,'','','','','','','','','','',34,7,'Church 2 User 2 (user22@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,1,'Super Admin',0,'Group \"Test Group 11\" Created:\nID - \"1\"\nname - \"Test Group 11\"\ndescription - \"Test Group\"\ngroup purpose - \"\"\nage range - \"\"\norganizer - \"\"\norganizer phone - \"\"\nlocation - \"\"\naddress - \"\"\ncity - \"\"\nstate - \"\"\nzip code - \"\"\nmeeting times - \"None\"\nchurch - \"Test Church 01, Guadala, Mexico\"\nGroup Kind - \"group\"\nadmins - []\nmembers - [Admin Church 01 (admin01@test.com), Church 1 User 1 (user11@test.com)]','2015-03-02 02:18:15','Admin Church 01 (admin01@test.com)\nChurch 1 User 1 (user11@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','Test Group 11','','','','','',23,1,'Test Group 11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-02 02:18:15','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(15,1,'Super Admin',1,'Group \"Test Group 11\" Changed:\nmeeting times - from \"None\" to \"DTSTART:20150301T204100-0600 RRULE:FREQ=DAILY;COUNT=1;BYHOUR=20;BYMINUTE=41;BYSECOND=0\"\nadmins - []\nmembers - [Admin Church 01 (admin01@test.com), Church 1 User 1 (user11@test.com)]','2015-03-02 02:41:36','Admin Church 01 (admin01@test.com)\nChurch 1 User 1 (user11@test.com)','','',NULL,'','','','','Test Group 11','','','','','',23,1,'Test Group 11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-02 02:41:36','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,1,'Super Admin',1,'Group \"Test Group 11\" Changed:\nmeeting times - from \"DTSTART:20150301T204100-0600 RRULE:FREQ=DAILY;COUNT=1;BYHOUR=20;BYMINUTE=41;BYSECOND=0\" to \"DTSTART:20150301T204100-0600 RRULE:FREQ=DAILY;COUNT=1;BYHOUR=20;BYMINUTE=41;BYSECOND=0\"\nadmins - []\nmembers - [Admin Church 01 (admin01@test.com), Church 1 User 1 (user11@test.com)]','2015-03-02 02:47:31','Admin Church 01 (admin01@test.com)\nChurch 1 User 1 (user11@test.com)','','',NULL,'','','','','Test Group 11','','','','','',23,1,'Test Group 11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-02 02:47:31','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,1,'Super Admin',1,'Group \"Test Group 11\" Changed:\nmeeting times - from \"DTSTART:20150301T204100-0600 RRULE:FREQ=DAILY;COUNT=1;BYHOUR=20;BYMINUTE=41;BYSECOND=0\" to \"DTSTART:20150301T204100-0600 RRULE:FREQ=DAILY;BYHOUR=20;BYMINUTE=41;BYSECOND=0\"\nadmins - []\nmembers - [Admin Church 01 (admin01@test.com), Church 1 User 1 (user11@test.com)]','2015-03-02 02:47:57','Admin Church 01 (admin01@test.com)\nChurch 1 User 1 (user11@test.com)','','',NULL,'','','','','Test Group 11','','','','','',23,1,'Test Group 11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(20,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-02 02:47:57','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(21,1,'Super Admin',0,'Group \"Test Group 12\" Created:\nID - \"2\"\nname - \"Test Group 12\"\ndescription - \"Another Group\"\ngroup purpose - \"\"\nage range - \"\"\norganizer - \"\"\norganizer phone - \"\"\nlocation - \"Here\"\naddress - \"\"\ncity - \"\"\nstate - \"\"\nzip code - \"\"\nmeeting times - \"None\"\nchurch - \"Test Church 01, Guadala, Mexico\"\nGroup Kind - \"group\"\nadmins - []\nmembers - []','2015-03-02 02:48:50','','Test Church 01, Guadala, Mexico','',NULL,'','','','','Test Group 12','','','','','',23,2,'Test Group 12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(22,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-02 02:48:50','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(23,1,'Super Admin',1,'Group \"Test Group 12\" Changed:\nadmins - []\nmembers - [Church 1 User 1 (user11@test.com), Church 1 User 2 (user12@test.com)]','2015-03-02 06:40:47','Church 1 User 1 (user11@test.com)\nChurch 1 User 2 (user12@test.com)','','',NULL,'','','','','Test Group 12','','','','','',23,2,'Test Group 12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(24,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-02 06:40:47','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(25,1,'Super Admin',0,'Group \"Group 21\" Created:\nID - \"3\"\nname - \"Group 21\"\ndescription - \"Group 21\"\ngroup purpose - \"\"\nage range - \"\"\norganizer - \"\"\norganizer phone - \"\"\nlocation - \"Somewhere\"\naddress - \"\"\ncity - \"\"\nstate - \"\"\nzip code - \"\"\nmeeting times - \"None\"\nchurch - \"Test Church 02, Fake, USA\"\nGroup Kind - \"group\"\nadmins - [Church 2 User 1 (user21@test.com)]\nmembers - []','2015-03-02 22:37:45','Church 2 User 1 (user21@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','Group 21','','','','','',23,3,'Group 21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,1,'Super Admin',0,'Group \"Group 22\" Created:\nID - \"4\"\nname - \"Group 22\"\ndescription - \"Group 22\"\ngroup purpose - \"\"\nage range - \"\"\norganizer - \"\"\norganizer phone - \"\"\nlocation - \"over the rainbow\"\naddress - \"\"\ncity - \"\"\nstate - \"\"\nzip code - \"\"\nmeeting times - \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\"\nchurch - \"Test Church 02, Fake, USA\"\nGroup Kind - \"group\"\nadmins - []\nmembers - []','2015-03-02 22:37:45','','Test Church 02, Fake, USA','',NULL,'','','','','Group 22','','','','','',23,4,'Group 22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(27,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-02 22:37:45','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(28,1,'Super Admin',1,'Group \"Group 21\" Changed:\nadmins - [Church 2 User 1 (user21@test.com)]\nmembers - [Church 2 User 1 (user21@test.com)]','2015-03-02 22:38:41','Church 2 User 1 (user21@test.com)','','',NULL,'','','','','Group 21','','','','','',23,3,'Group 21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(29,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-02 22:38:41','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(30,1,'Super Admin',1,'Group \"Group 22\" Changed:\nmeeting times - from \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=MO;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\" to \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\"\nadmins - []\nmembers - []','2015-03-02 22:39:26','','','',NULL,'','','','','Group 22','','','','','',23,4,'Group 22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(31,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-02 22:39:26','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(32,1,'Super Admin',1,'Group \"Group 22\" Changed:\nmeeting times - from \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=MO;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\" to \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=MO,WE,FR;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\"\nadmins - []\nmembers - []','2015-03-02 22:57:11','','','',NULL,'','','','','Group 22','','','','','',23,4,'Group 22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(33,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-02 22:57:11','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,1,'Super Admin',1,'Group \"Group 22\" Changed:\nmeeting times - from \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=MO,WE,FR;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\" to \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=WE,FR;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\"\nadmins - []\nmembers - []','2015-03-02 22:57:58','','','',NULL,'','','','','Group 22','','','','','',23,4,'Group 22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(35,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-02 22:57:58','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(36,1,'Super Admin',1,'Group \"Group 22\" Changed:\nmeeting times - from \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=WE,FR;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\" to \"DTSTART:20150302T163700-0600 RRULE:FREQ=WEEKLY;BYWEEKDAY=MO,WE,FR;INTERVAL=2;COUNT=8;BYHOUR=16;BYMINUTE=37;BYSECOND=0\"\nadmins - []\nmembers - []','2015-03-02 22:59:20','','','',NULL,'','','','','Group 22','','','','','',23,4,'Group 22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(37,1,'Super Admin',1,'Group \"Group 21\" Changed:\nmeeting times - from \"None\" to \"DTSTART:20150317T120000-0500 RRULE:FREQ=MONTHLY;UNTIL=20151217T120000-0600;BYHOUR=12;BYMINUTE=0;BYSECOND=0\"\nadmins - [Church 2 User 1 (user21@test.com)]\nmembers - [Church 2 User 1 (user21@test.com)]','2015-03-02 22:59:20','Church 2 User 1 (user21@test.com)','','',NULL,'','','','','Group 21','','','','','',23,3,'Group 21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(38,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-02 22:59:20','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(39,1,'Super Admin',1,'Group \"Group 21\" Changed:\nmeeting times - from \"DTSTART:20150317T120000-0500 RRULE:FREQ=MONTHLY;UNTIL=20151217T120000-0600;BYMONTHDAY=17;BYHOUR=12;BYMINUTE=0;BYSECOND=0\" to \"DTSTART:20150317T120000-0500 RRULE:FREQ=MONTHLY;UNTIL=20151217T120000-0600;COUNT=12;BYHOUR=12;BYMINUTE=0;BYSECOND=0\"\nadmins - [Church 2 User 1 (user21@test.com)]\nmembers - [Church 2 User 1 (user21@test.com)]','2015-03-02 22:59:49','Church 2 User 1 (user21@test.com)','','',NULL,'','','','','Group 21','','','','','',23,3,'Group 21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(40,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-02 22:59:49','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(41,1,'Super Admin',0,'User \"Faith Admin (admin@test.com)\" Created:\nID - \"9\"\npassword - \"pbkdf2_sha256$12000$JIDHooplqVob$9UNPa76xDf24cjKMyv9yY0vSoDPRv0c2xwUf4NY37u4=\"\nlast login - \"2015-03-11 00:00:11.801841+00:00\"\nsuperuser status - \"False\"\nemail address - \"admin@test.com\"\ntitle - \"Mr.\"\nfirst name - \"Faith\"\nlast name - \"Admin\"\nmarital status - \"Single\"\ncountry - \"Test\"\nstate - \"Test\"\ncity - \"Test\"\naddress - \"Test\"\nzip code - \"\"\ntelephone number - \"\"\nSubscribed - \"False\"\ndecryption key - \"-=nsbEwPh8WF-e\"\nstaff status - \"False\"\nactive - \"True\"\ndate joined - \"2015-03-11 00:00:11.801886+00:00\"\nsecurity question - \"0\"\nsecurity answer - \"test\"\nstore credit - \"0\"\nChurch or Worship center - \"Non-Affiliated\"\nfamily group - \"None\"\ngroups - [Consumer]\nuser permissions - []','2015-03-11 00:00:11','Faith Admin (admin@test.com)','Non-Affiliated','',NULL,'','','','','','','','','','',34,9,'Faith Admin (admin@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(42,1,'Super Admin',1,'User \"Faith Admin (admin@test.com)\" Changed:\nlast login - from \"2015-03-11 00:00:11+00:00\" to \"2015-03-10 19:00:00-05:00\"\nstaff status - from \"False\" to \"True\"\ndate joined - from \"2015-03-11 00:00:11+00:00\" to \"2015-03-10 19:00:00-05:00\"\ngroups - [Faith Media Administrator]\nuser permissions - []','2015-03-11 00:00:33','Faith Admin (admin@test.com)','','',NULL,'','','','','','','','','','',34,9,'Faith Admin (admin@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(43,1,'Super Admin',0,'User \"Customer Service (service@test.com)\" Created:\nID - \"10\"\npassword - \"pbkdf2_sha256$12000$9F8x7VAV1axm$Mc5XeDQ/TbPa1BiL/zwHOICiWp7tBllp/eKVbTqWV6A=\"\nlast login - \"2015-03-11 00:01:07.554548+00:00\"\nsuperuser status - \"False\"\nemail address - \"service@test.com\"\ntitle - \"Mr.\"\nfirst name - \"Customer\"\nlast name - \"Service\"\nmarital status - \"Single\"\ncountry - \"Test\"\nstate - \"Test\"\ncity - \"Test\"\naddress - \"Test\"\nzip code - \"\"\ntelephone number - \"\"\nSubscribed - \"False\"\ndecryption key - \"::+B:Xpxm^XdOf@R\"\nstaff status - \"False\"\nactive - \"True\"\ndate joined - \"2015-03-11 00:01:07.554613+00:00\"\nsecurity question - \"0\"\nsecurity answer - \"test\"\nstore credit - \"0\"\nChurch or Worship center - \"Non-Affiliated\"\nfamily group - \"None\"\ngroups - [Consumer]\nuser permissions - []','2015-03-11 00:01:07','Customer Service (service@test.com)','Non-Affiliated','',NULL,'','','','','','','','','','',34,10,'Customer Service (service@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(44,1,'Super Admin',1,'User \"Customer Service (service@test.com)\" Changed:\nlast login - from \"2015-03-11 00:01:07+00:00\" to \"2015-03-10 19:01:00-05:00\"\nstaff status - from \"False\" to \"True\"\ndate joined - from \"2015-03-11 00:01:07+00:00\" to \"2015-03-10 19:01:00-05:00\"\ngroups - [Customer Service]\nuser permissions - []','2015-03-11 00:01:25','Customer Service (service@test.com)','','',NULL,'','','','','','','','','','',34,10,'Customer Service (service@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(45,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nlast login - from \"2015-03-02 23:21:58+00:00\" to \"2015-03-02 17:21:00-06:00\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 00:47:12','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(46,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 00:48:03','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(47,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 00:48:20','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(48,2,'Admin Church 01',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nChurch or Worship center - from \"Test Church 01, Guadala, Mexico\" to \"Test Church 02, Fake, USA\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 00:49:46','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico\nTest Church 02, Fake, USA','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(49,2,'Admin Church 01',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nChurch or Worship center - from \"Test Church 02, Fake, USA\" to \"Test Church 01, Guadala, Mexico\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 01:19:08','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico\nTest Church 02, Fake, USA','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(50,1,'Super Admin',1,'User \"Admin for Church 01 (admin01@test.com)\" Changed:\nlast login - from \"2015-03-11 00:48:52+00:00\" to \"2015-03-10 19:48:00-05:00\"\nfirst name - from \"Admin\" to \"Admin for\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 01:51:29','Admin for Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin for Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(51,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nfirst name - from \"Admin for\" to \"Admin\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 01:52:39','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nlast login - from \"2015-03-11 03:15:27+00:00\" to \"2015-03-10 22:15:00-05:00\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:16:35','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(53,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nlast login - from \"2015-03-11 03:16:44+00:00\" to \"2015-03-10 22:16:00-05:00\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:17:17','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(54,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nChurch or Worship center - from \"Test Church 01, Guadala, Mexico\" to \"Test Church 02, Fake, USA\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:17:51','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico\nTest Church 02, Fake, USA','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(55,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nChurch or Worship center - from \"Test Church 02, Fake, USA\" to \"Test Church 01, Guadala, Mexico\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:18:01','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico\nTest Church 02, Fake, USA','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(56,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:25:13','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(57,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:26:33','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(58,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:26:43','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(59,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:26:59','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(60,1,'Super Admin',1,'User \"Admin for Church 01 (admin01@test.com)\" Changed:\nfirst name - from \"Admin\" to \"Admin for\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:27:06','Admin for Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin for Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(61,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nfirst name - from \"Admin for\" to \"Admin\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:27:16','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(62,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:28:04','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(63,1,'Super Admin',1,'User \"Admin Church 01 (admin01@test.com)\" Changed:\nlast login - from \"2015-03-11 03:28:13+00:00\" to \"2015-03-10 22:28:00-05:00\"\ngroups - [Church Administrator]\nuser permissions - []','2015-03-11 03:28:43','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','',34,2,'Admin Church 01 (admin01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(64,1,'Super Admin',0,'Device \"iPhone - Test iPhone\" Created:\nID - \"1\"\nkind of device - \"2\"\ndevice name - \"Test iPhone\"\nidentification number - \"as;kldjqw9e\"\nowner - \"No Church User 1 (user01@test.com)\"\ndecryption key - \"^Dm@m-75ob=-Gsgc\"','2015-03-11 16:09:34','No Church User 1 (user01@test.com)','','',NULL,'','','','','','','','','','',35,1,'iPhone - Test iPhone',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(65,1,'Super Admin',1,'User \"No Church User 1 (user01@test.com)\" Changed:\nlast login - from \"2015-02-26 03:59:37+00:00\" to \"2015-02-25 21:59:00-06:00\"\ngroups - [Consumer]\nuser permissions - []','2015-03-11 16:09:34','No Church User 1 (user01@test.com)','','',NULL,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(66,1,'Super Admin',0,'Device \"iPad - Test iPad\" Created:\nID - \"2\"\nkind of device - \"3\"\ndevice name - \"Test iPad\"\nidentification number - \"24n234tm3h;lk2u3\"\nowner - \"Church 1 User 1 (user11@test.com)\"\ndecryption key - \"wHv_6h--aH_Pzoc\"','2015-03-11 16:16:47','Church 1 User 1 (user11@test.com)','','',NULL,'','','','','','','','','','',35,2,'iPad - Test iPad',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(67,1,'Super Admin',1,'User \"Church 1 User 1 (user11@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-03-11 16:16:47','Church 1 User 1 (user11@test.com)','','',NULL,'','','','','','','','','','',34,4,'Church 1 User 1 (user11@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(68,1,'Super Admin',0,'Location \"Main Hall\" Created:\nID - \"1\"\nname - \"Main Hall\"\ncountry - \"Test\"\nstate - \"Test\"\ncity - \"Test\"\naddress - \"Test\"\nzip code - \"\"\nchurch - \"Test Church 01, Guadala, Mexico\"','2015-03-11 16:20:48','','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','Main Hall','','',25,1,'Main Hall',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(69,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-11 16:20:48','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(70,1,'Super Admin',0,'Check in \"2015-03-11 16:21:11.415759+00:00: Main Hall\" Created:\nID - \"1\"\nuser - \"Church 1 User 1 (user11@test.com)\"\nlocation - \"Main Hall\"\ndetails - \"Alone\"\ntime - \"2015-03-11 16:21:11.415759+00:00\"','2015-03-11 16:21:11','Church 1 User 1 (user11@test.com)','','',NULL,'','','','','','','','Main Hall','','',37,1,'2015-03-11 16:21:11.415759+00:00: Main Hall',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(71,1,'Super Admin',1,'User \"Church 1 User 1 (user11@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-03-11 16:21:11','Church 1 User 1 (user11@test.com)','','',NULL,'','','','','','','','','','',34,4,'Church 1 User 1 (user11@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(72,1,'Super Admin',1,'Event \"Wed, 18 Mar 2015 - Event 01\" Changed:\nevent times - from \"DTSTART:20150225T142200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=22;BYSECOND=0\" to \"DTSTART:20150225T142200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=22;BYSECOND=0\"\nregistered - [Admin Church 01 (admin01@test.com)]','2015-03-18 19:59:52','Admin Church 01 (admin01@test.com)','','',NULL,'','','','','','','','','','Wed, 18 Mar 2015 - Event 01',22,1,'Wed, 18 Mar 2015 - Event 01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(73,1,'Super Admin',1,'Event \"Wed, 18 Mar 2015 - Event 02\" Changed:\nevent times - from \"DTSTART:20150225T142200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=22;BYSECOND=0\" to \"DTSTART:20150225T142200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=22;BYSECOND=0\"\nregistered - [Church 1 User 1 (user11@test.com), Church 1 User 2 (user12@test.com)]','2015-03-18 19:59:52','Church 1 User 1 (user11@test.com)\nChurch 1 User 2 (user12@test.com)','','',NULL,'','','','','','','','','','Wed, 18 Mar 2015 - Event 02',22,2,'Wed, 18 Mar 2015 - Event 02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(74,1,'Super Admin',1,'Church \"Test Church 01, Guadala, Mexico\" Changed:\nChurch timezone - from \"US/Central\" to \"America/Monterrey\"\neditors - [Admin Church 01 (admin01@test.com)]','2015-03-18 19:59:52','Admin Church 01 (admin01@test.com)','Test Church 01, Guadala, Mexico','',NULL,'','','','','','','','','','',20,2,'Test Church 01, Guadala, Mexico',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(75,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\nChurch timezone - from \"US/Central\" to \"Asia/Kolkata\"\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-18 20:01:01','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(76,1,'Super Admin',1,'Event \"Wed, 25 Mar 2015 - Event 01\" Changed:\nevent times - from \"DTSTART:20150225T144200-0600 RRULE:FREQ=DAILY;BYHOUR=14;BYMINUTE=42;BYSECOND=0\" to \"DTSTART:20150325T194500+0530 RRULE:FREQ=DAILY;BYHOUR=19;BYMINUTE=45;BYSECOND=0\"\nregistered - [Church 2 User 1 (user21@test.com)]','2015-03-23 01:45:27','Church 2 User 1 (user21@test.com)','','',NULL,'','','','','','','','','','Wed, 25 Mar 2015 - Event 01',22,3,'Wed, 25 Mar 2015 - Event 01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(77,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-23 01:45:27','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(78,1,'Super Admin',1,'Church \"Test Church 02, Fake, USA\" Changed:\nChurch timezone - from \"Asia/Kolkata\" to \"US/Central\"\neditors - [Admin Church 02 (admin02@test.com)]','2015-03-23 02:17:28','Admin Church 02 (admin02@test.com)','Test Church 02, Fake, USA','',NULL,'','','','','','','','','','',20,3,'Test Church 02, Fake, USA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(79,1,'Super Admin',1,'User \"No Church User 1 (user01@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-04-30 03:18:33','No Church User 1 (user01@test.com)','','',NULL,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(80,1,'Super Admin',0,'Movie \"Movie - \" Created:\nID - \"1\"\nmedia ID - \"mos2014\"\ntitle - \"\"\nsynopsis - \"\"\ncover art - \"\"\nmedia file - \"\"\nuploaded at - \"2015-05-02 02:24:14.101164+00:00\"\nuploader - \"None\"\nrelease date - \"2015-05-02 02:24:14.101180+00:00\"\ndecryption key - \"x:iSk8z@05Y^4U\"\nrating - \"None\"\nduration - \"None\"\npurchased by - []\navailable on church - []\ncategories - []\ndirector(s) - []\ncast - []','2015-05-02 02:24:14','','','',NULL,'Movie - ','','','','','','','','','',16,1,'Movie - ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(81,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\ntitle - from \"\" to \"Man Of Steel\"\ncover art - from \"\" to \"Movie/mos2014/cover.png\"\nmedia file - from \"\" to \"Movie/mos2014/mos2014.mp4\"\nuploaded at - from \"2015-05-02 02:24:14+00:00\" to \"2015-05-01 21:24:00-05:00\"\nrelease date - from \"2015-05-02 02:24:14+00:00\" to \"2015-05-01 21:24:00-05:00\"\npurchased by - []\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-02 02:26:36','','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(82,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - []\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-02 02:31:22','','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(83,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - []\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-03 18:15:53','','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(84,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - []\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-03 18:15:58','','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(85,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - []\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-03 18:16:13','','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(86,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - []\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-03 18:16:21','','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(87,1,'Super Admin',1,'Device \"iPhone - Test iPhone\" Changed:\nidentification number - from \"as;kldjqw9e\" to \"abcd\"','2015-05-04 02:45:07','','','',NULL,'','','','','','','','','','',35,1,'iPhone - Test iPhone',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(88,1,'Super Admin',1,'User \"No Church User 1 (user01@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-05-04 02:45:07','No Church User 1 (user01@test.com)','','',NULL,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(89,1,'Super Admin',1,'Device \"iPhone - Same iPhone\" Changed:\nkind of device - from \"3\" to \"2\"\ndevice name - from \"Test iPad\" to \"Same iPhone\"\nidentification number - from \"24n234tm3h;lk2u3\" to \"abcd\"','2015-05-04 02:45:30','','','',NULL,'','','','','','','','','','',35,2,'iPhone - Same iPhone',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(90,1,'Super Admin',1,'User \"Church 1 User 1 (user11@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-05-04 02:45:30','Church 1 User 1 (user11@test.com)','','',NULL,'','','','','','','','','','',34,4,'Church 1 User 1 (user11@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(91,1,'Super Admin',0,'User content \"Movie - Man Of Steel\" Created:\nID - \"5\"\nuser - \"No Church User 1 (user01@test.com)\"\nbook - \"None\"\nmovie - \"Movie - Man Of Steel\"\nsong - \"None\"\nsermon - \"None\"\ngift - \"None\"\nkind of purchase - \"0\"\ndate of purchase - \"2015-05-03 21:46:00-05:00\"\nvalid until - \"None\"\ndecryption key - \"45v:Ep6_tM%2:@d1\"\ndevices for reproduction - []','2015-05-04 02:46:21','No Church User 1 (user01@test.com)','','',NULL,'Movie - Man Of Steel','','','','','','','','','',8,5,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(92,1,'Super Admin',1,'User \"No Church User 1 (user01@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-05-04 02:46:21','No Church User 1 (user01@test.com)','','',NULL,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(93,1,'Super Admin',0,'User content \"Movie - Man Of Steel\" Created:\nID - \"6\"\nuser - \"Church 1 User 1 (user11@test.com)\"\nbook - \"None\"\nmovie - \"Movie - Man Of Steel\"\nsong - \"None\"\nsermon - \"None\"\ngift - \"None\"\nkind of purchase - \"0\"\ndate of purchase - \"2015-05-03 21:46:00-05:00\"\nvalid until - \"None\"\ndecryption key - \"vfQ@Nt^VqTTyt\"\ndevices for reproduction - []','2015-05-04 02:46:48','Church 1 User 1 (user11@test.com)','','',NULL,'Movie - Man Of Steel','','','','','','','','','',8,6,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(94,1,'Super Admin',1,'User \"Church 1 User 1 (user11@test.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-05-04 02:46:48','Church 1 User 1 (user11@test.com)','','',NULL,'','','','','','','','','','',34,4,'Church 1 User 1 (user11@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(95,1,'Super Admin',0,'Price \"$14, ( - )\" Created:\nID - \"2\"\nrent - \"False\"\nrent time - \"0\"\nprice - \"14\"\nspecial sale - \"False\"\nvalid from - \"None\"\nvalid untill - \"None\"\ncontent type - \"movie\"\nobject id - \"1\"','2015-05-22 15:10:45','','','',NULL,'','','','','','','','','','',14,2,'$14, ( - )',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(96,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - [No Church User 1 (user01@test.com), Church 1 User 1 (user11@test.com)]\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-22 15:10:45','No Church User 1 (user01@test.com)\nChurch 1 User 1 (user11@test.com)','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(97,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - [No Church User 1 (user01@test.com), Church 1 User 1 (user11@test.com)]\navailable on church - []\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-22 15:10:51','No Church User 1 (user01@test.com)\nChurch 1 User 1 (user11@test.com)','','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(98,1,'Super Admin',1,'Movie \"Movie - Man Of Steel\" Changed:\npurchased by - [No Church User 1 (user01@test.com), Church 1 User 1 (user11@test.com)]\navailable on church - [Non-Affiliated, Test Church 01, Guadala, Mexico, Test Church 02, Fake, USA, Empty, emtpy, empty]\ncategories - []\ndirector(s) - [Test Author]\ncast - []','2015-05-22 15:33:27','No Church User 1 (user01@test.com)\nChurch 1 User 1 (user11@test.com)','Non-Affiliated\nTest Church 01, Guadala, Mexico\nTest Church 02, Fake, USA\nEmpty, emtpy, empty','',NULL,'Movie - Man Of Steel','','','','','','','','','',16,1,'Movie - Man Of Steel',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(99,1,'Super Admin',0,'Gift \"Gift - \" Created:\nID - \"1\"\nmedia ID - \"gift01\"\ntitle - \"\"\nsynopsis - \"\"\ncover art - \"\"\nmedia file - \"\"\nuploaded at - \"2015-06-04 20:51:36.930279+00:00\"\nuploader - \"None\"\nrelease date - \"2015-06-04 20:51:36.930295+00:00\"\ndecryption key - \"WN=%:M=tZJgilGs\"\nrating - \"None\"\npurchased by - []\navailable on church - []\ncategories - []\nvendor(s) - []','2015-06-04 20:51:36','','','',NULL,'','','','Gift - ','','','','','','',19,1,'Gift - ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(100,1,'Super Admin',0,'Price \"$14, ( - )\" Created:\nID - \"3\"\nrent - \"False\"\nrent time - \"0\"\nprice - \"14\"\nspecial sale - \"False\"\nvalid from - \"None\"\nvalid untill - \"None\"\ncontent type - \"gift\"\nobject id - \"1\"','2015-06-04 20:52:27','','','',NULL,'','','','','','','','','','',14,3,'$14, ( - )',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(101,1,'Super Admin',1,'Gift \"Gift - Test Gift\" Changed:\ntitle - from \"\" to \"Test Gift\"\nsynopsis - from \"\" to \"Something to sell\"\ncover art - from \"\" to \"Gift/gift01/cover.png\"\nuploaded at - from \"2015-06-04 20:51:36+00:00\" to \"2015-06-04 15:51:00-05:00\"\nrelease date - from \"2015-06-04 20:51:36+00:00\" to \"2015-06-04 15:51:00-05:00\"\nrating - from \"None\" to \"PG\"\npurchased by - []\navailable on church - [Non-Affiliated, Test Church 01, Guadala, Mexico, Test Church 02, Fake, USA, Empty, emtpy, empty]\ncategories - [Family]\nvendor(s) - []','2015-06-04 20:52:27','','Non-Affiliated\nTest Church 01, Guadala, Mexico\nTest Church 02, Fake, USA\nEmpty, emtpy, empty','',NULL,'','','','Gift - Test Gift','','','','','','',19,1,'Gift - Test Gift',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(102,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui Garcia\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 13:59:52','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(103,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui Garcia\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 14:01:56','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(104,1,'Super Admin',1,'User \"Yamanqui Garcia (yamanqui@gmail.com)\" Changed:\nlast login - from \"2015-07-27 14:22:57+00:00\" to \"2015-07-27 09:22:00-05:00\"\nlast name - from \"García\" to \"Garcia\"\ndate joined - from \"2015-07-27 14:22:57+00:00\" to \"2015-07-27 09:22:00-05:00\"\ngroups - [Consumer]\nuser permissions - []','2015-07-27 14:27:54','Yamanqui Garcia (yamanqui@gmail.com)','','',NULL,'','','','','','','','','','',34,11,'Yamanqui Garcia (yamanqui@gmail.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(105,1,'Super Admin',2,'User content \"Book - Test Book\" Deleted:\nID - \"None\"\nuser - \"Yamanqui Garcia (yamanqui@gmail.com)\"\nbook - \"Book - Test Book\"\nmovie - \"None\"\nsong - \"None\"\nsermon - \"None\"\ngift - \"None\"\nkind of purchase - \"0\"\ndate of purchase - \"2015-07-27 09:27:00-05:00\"\nvalid until - \"None\"\ndecryption key - \"6xV=Zw=D3pd=Lm\"','2015-07-27 14:44:50','Yamanqui Garcia (yamanqui@gmail.com)','','Book - Test Book',NULL,'','','','','','','','','','',8,NULL,'Book - Test Book',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(106,1,'Super Admin',1,'User \"Yamanqui Garcia (yamanqui@gmail.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-07-27 14:44:50','Yamanqui Garcia (yamanqui@gmail.com)','','',NULL,'','','','','','','','','','',34,11,'Yamanqui Garcia (yamanqui@gmail.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(107,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui García Rosales\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 14:49:57','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(108,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui García Rosales\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 14:52:07','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(109,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui García Rosales\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 14:55:55','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(110,12,'Yamanqui García Rosales',0,'User \"Yamanqui García Rosales (yamanqui@gmail.com)\" Created:\nID - \"12\"\npassword - \"pbkdf2_sha256$12000$4ics6eHKQN94$HsmQW1DGByE1WnJF2UvHNhpxBHr5uNiSWz1E/y85dQo=\"\nlast login - \"2015-07-27 15:00:43.900721+00:00\"\nsuperuser status - \"False\"\nemail address - \"yamanqui@gmail.com\"\ntitle - \"Mr.\"\nfirst name - \"Yamanqui\"\nlast name - \"García Rosales\"\nmarital status - \"Married\"\ncountry - \"México\"\nstate - \"Nuevo Leon\"\ncity - \"Monterrey\"\naddress - \"Río Nilo 229 A, Col. Roma\"\nzip code - \"64700\"\ntelephone number - \"(81) 8359 5395\"\nSubscribed - \"False\"\nbirth date - \"1980-11-14\"\nsex - \"male\"\ndecryption key - \"+l_g%sSHHN^P+p\"\nstaff status - \"False\"\nactive - \"True\"\ndate joined - \"2015-07-27 15:00:43.901008+00:00\"\nsecurity question - \"0\"\nsecurity answer - \"yamis\"\nstore credit - \"0\"\nChurch or Worship center - \"Non-Affiliated\"\nfamily group - \"None\"\ngroups - [Consumer]\nuser permissions - []','2015-07-27 15:00:44','Yamanqui García Rosales (yamanqui@gmail.com)','Non-Affiliated','',NULL,'','','','','','','','','','',34,12,'Yamanqui García Rosales (yamanqui@gmail.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(111,1,'Super Admin',2,'User content \"Book - Test Book\" Deleted:\nID - \"None\"\nuser - \"Yamanqui García Rosales (yamanqui@gmail.com)\"\nbook - \"Book - Test Book\"\nmovie - \"None\"\nsong - \"None\"\nsermon - \"None\"\ngift - \"None\"\nkind of purchase - \"0\"\ndate of purchase - \"2015-07-27 10:00:00-05:00\"\nvalid until - \"None\"\ndecryption key - \"Iw_J=H3ji@GO@-ed\"','2015-07-27 15:01:09','Yamanqui García Rosales (yamanqui@gmail.com)','','Book - Test Book',NULL,'','','','','','','','','','',8,NULL,'Book - Test Book',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(112,1,'Super Admin',1,'User \"Yamanqui García Rosales (yamanqui@gmail.com)\" Changed:\nlast login - from \"2015-07-27 15:00:43+00:00\" to \"2015-07-27 10:00:00-05:00\"\ndate joined - from \"2015-07-27 15:00:43+00:00\" to \"2015-07-27 10:00:00-05:00\"\ngroups - [Consumer]\nuser permissions - []','2015-07-27 15:01:09','Yamanqui García Rosales (yamanqui@gmail.com)','','',NULL,'','','','','','','','','','',34,12,'Yamanqui García Rosales (yamanqui@gmail.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(113,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui García Rosales\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 15:02:18','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(114,1,'Super Admin',2,'User content \"Book - Test Book\" Deleted:\nID - \"None\"\nuser - \"Yamanqui García Rosales (yamanqui@gmail.com)\"\nbook - \"Book - Test Book\"\nmovie - \"None\"\nsong - \"None\"\nsermon - \"None\"\ngift - \"None\"\nkind of purchase - \"0\"\ndate of purchase - \"2015-07-27 10:02:00-05:00\"\nvalid until - \"None\"\ndecryption key - \"%@cuUvn1z9_FOh\"','2015-07-27 15:04:31','Yamanqui García Rosales (yamanqui@gmail.com)','','Book - Test Book',NULL,'','','','','','','','','','',8,NULL,'Book - Test Book',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(115,1,'Super Admin',1,'User \"Yamanqui García Rosales (yamanqui@gmail.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-07-27 15:04:31','Yamanqui García Rosales (yamanqui@gmail.com)','','',NULL,'','','','','','','','','','',34,12,'Yamanqui García Rosales (yamanqui@gmail.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(116,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui García Rosales\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 15:05:22','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(117,1,'Super Admin',2,'User content \"Book - Test Book\" Deleted:\nID - \"None\"\nuser - \"Yamanqui García Rosales (yamanqui@gmail.com)\"\nbook - \"Book - Test Book\"\nmovie - \"None\"\nsong - \"None\"\nsermon - \"None\"\ngift - \"None\"\nkind of purchase - \"0\"\ndate of purchase - \"2015-07-27 10:05:00-05:00\"\nvalid until - \"None\"\ndecryption key - \"ZIHfXC^@=:QFc=3g\"','2015-07-27 15:06:25','Yamanqui García Rosales (yamanqui@gmail.com)','','Book - Test Book',NULL,'','','','','','','','','','',8,NULL,'Book - Test Book',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(118,1,'Super Admin',1,'User \"Yamanqui García Rosales (yamanqui@gmail.com)\" Changed:\ngroups - [Consumer]\nuser permissions - []','2015-07-27 15:06:25','Yamanqui García Rosales (yamanqui@gmail.com)','','',NULL,'','','','','','','','','','',34,12,'Yamanqui García Rosales (yamanqui@gmail.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(119,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui García Rosales\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 15:07:05','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL),(120,8,'No Church User 1',3,'No Church User 1 Check out:\n$341.12 - Purchase: Book - Test Book to Yamanqui García Rosales\n----------\nSub Total: $341.12\n      Tax: $ 28.14\n    Total: $369.26','2015-07-27 15:10:12','No Church User 1 (user01@test.com)','Non-Affiliated','Book - Test Book',369.26,'','','','','','','','','','',34,8,'No Church User 1 (user01@test.com)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,341.12,28.14,NULL);
/*!40000 ALTER TABLE `archive_logentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_books`
--

DROP TABLE IF EXISTS `archive_logentry_books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_books_logentry_id_101953fe32f571_uniq` (`logentry_id`,`book_id`),
  KEY `archive_logentry_books_74be55db` (`logentry_id`),
  KEY `archive_logentry_books_36c249d7` (`book_id`),
  CONSTRAINT `book_id_refs_id_41744787` FOREIGN KEY (`book_id`) REFERENCES `content_book` (`id`),
  CONSTRAINT `logentry_id_refs_id_acf25b29` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_books`
--

LOCK TABLES `archive_logentry_books` WRITE;
/*!40000 ALTER TABLE `archive_logentry_books` DISABLE KEYS */;
INSERT INTO `archive_logentry_books` VALUES (1,102,1),(2,103,1),(3,107,1),(4,108,1),(5,109,1),(6,113,1),(7,116,1),(8,119,1),(9,120,1);
/*!40000 ALTER TABLE `archive_logentry_books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_churches`
--

DROP TABLE IF EXISTS `archive_logentry_churches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_churches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `church_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_churches_logentry_id_6ed5bcf3ddc0469f_uniq` (`logentry_id`,`church_id`),
  KEY `archive_logentry_churches_74be55db` (`logentry_id`),
  KEY `archive_logentry_churches_60d6ec24` (`church_id`),
  CONSTRAINT `church_id_refs_id_9c908b99` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`),
  CONSTRAINT `logentry_id_refs_id_108aeed0` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_churches`
--

LOCK TABLES `archive_logentry_churches` WRITE;
/*!40000 ALTER TABLE `archive_logentry_churches` DISABLE KEYS */;
INSERT INTO `archive_logentry_churches` VALUES (1,1,3),(2,2,4),(3,3,2),(4,4,2),(5,7,3),(6,8,3),(7,9,3),(8,13,2),(9,14,2),(10,16,2),(11,18,2),(12,20,2),(13,21,2),(14,22,2),(15,24,2),(16,25,3),(17,26,3),(18,27,3),(19,29,3),(20,31,3),(21,33,3),(22,35,3),(23,38,3),(24,40,3),(25,41,1),(26,43,1),(27,48,2),(28,48,3),(29,49,2),(30,49,3),(31,54,2),(32,54,3),(33,55,2),(34,55,3),(35,68,2),(36,69,2),(37,74,2),(38,75,3),(39,77,3),(40,78,3),(41,98,1),(42,98,2),(43,98,3),(44,98,4),(45,101,1),(46,101,2),(47,101,3),(48,101,4),(49,102,1),(50,103,1),(51,107,1),(52,108,1),(53,109,1),(54,113,1),(55,116,1),(56,119,1),(57,120,1);
/*!40000 ALTER TABLE `archive_logentry_churches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_events`
--

DROP TABLE IF EXISTS `archive_logentry_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_events_logentry_id_6745efbee26c9d19_uniq` (`logentry_id`,`event_id`),
  KEY `archive_logentry_events_74be55db` (`logentry_id`),
  KEY `archive_logentry_events_a41e20fe` (`event_id`),
  CONSTRAINT `event_id_refs_id_aa25e753` FOREIGN KEY (`event_id`) REFERENCES `affiliation_event` (`id`),
  CONSTRAINT `logentry_id_refs_id_b1de57a0` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_events`
--

LOCK TABLES `archive_logentry_events` WRITE;
/*!40000 ALTER TABLE `archive_logentry_events` DISABLE KEYS */;
INSERT INTO `archive_logentry_events` VALUES (1,72,1),(2,73,2),(3,76,3);
/*!40000 ALTER TABLE `archive_logentry_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_family_groups`
--

DROP TABLE IF EXISTS `archive_logentry_family_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_family_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `familygroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_family_group_logentry_id_4e0138610e9d0c47_uniq` (`logentry_id`,`familygroup_id`),
  KEY `archive_logentry_family_groups_74be55db` (`logentry_id`),
  KEY `archive_logentry_family_groups_2963d102` (`familygroup_id`),
  CONSTRAINT `familygroup_id_refs_id_bf67b667` FOREIGN KEY (`familygroup_id`) REFERENCES `affiliation_familygroup` (`id`),
  CONSTRAINT `logentry_id_refs_id_507b721a` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_family_groups`
--

LOCK TABLES `archive_logentry_family_groups` WRITE;
/*!40000 ALTER TABLE `archive_logentry_family_groups` DISABLE KEYS */;
INSERT INTO `archive_logentry_family_groups` VALUES (1,3,1),(2,5,1),(3,6,1),(4,7,2),(5,8,3),(6,10,2),(7,11,3);
/*!40000 ALTER TABLE `archive_logentry_family_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_gifts`
--

DROP TABLE IF EXISTS `archive_logentry_gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_gifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `gift_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_gifts_logentry_id_61b0959da7304665_uniq` (`logentry_id`,`gift_id`),
  KEY `archive_logentry_gifts_74be55db` (`logentry_id`),
  KEY `archive_logentry_gifts_46862fea` (`gift_id`),
  CONSTRAINT `gift_id_refs_id_19f786d4` FOREIGN KEY (`gift_id`) REFERENCES `content_gift` (`id`),
  CONSTRAINT `logentry_id_refs_id_0af41e67` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_gifts`
--

LOCK TABLES `archive_logentry_gifts` WRITE;
/*!40000 ALTER TABLE `archive_logentry_gifts` DISABLE KEYS */;
INSERT INTO `archive_logentry_gifts` VALUES (1,99,1),(2,101,1);
/*!40000 ALTER TABLE `archive_logentry_gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_givings`
--

DROP TABLE IF EXISTS `archive_logentry_givings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_givings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `giving_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_givings_logentry_id_43e282175d2522b_uniq` (`logentry_id`,`giving_id`),
  KEY `archive_logentry_givings_74be55db` (`logentry_id`),
  KEY `archive_logentry_givings_707f38f4` (`giving_id`),
  CONSTRAINT `giving_id_refs_id_328b37dc` FOREIGN KEY (`giving_id`) REFERENCES `affiliation_giving` (`id`),
  CONSTRAINT `logentry_id_refs_id_38348268` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_givings`
--

LOCK TABLES `archive_logentry_givings` WRITE;
/*!40000 ALTER TABLE `archive_logentry_givings` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_logentry_givings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_groups`
--

DROP TABLE IF EXISTS `archive_logentry_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_groups_logentry_id_7e82c41485c5223b_uniq` (`logentry_id`,`group_id`),
  KEY `archive_logentry_groups_74be55db` (`logentry_id`),
  KEY `archive_logentry_groups_5f412f9a` (`group_id`),
  CONSTRAINT `group_id_refs_id_1e797ef8` FOREIGN KEY (`group_id`) REFERENCES `affiliation_group` (`id`),
  CONSTRAINT `logentry_id_refs_id_335419d9` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_groups`
--

LOCK TABLES `archive_logentry_groups` WRITE;
/*!40000 ALTER TABLE `archive_logentry_groups` DISABLE KEYS */;
INSERT INTO `archive_logentry_groups` VALUES (1,13,1),(2,15,1),(3,17,1),(4,19,1),(5,21,2),(6,23,2),(7,25,3),(8,26,4),(9,28,3),(10,30,4),(11,32,4),(12,34,4),(13,36,4),(14,37,3),(15,39,3);
/*!40000 ALTER TABLE `archive_logentry_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_locations`
--

DROP TABLE IF EXISTS `archive_logentry_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_locations_logentry_id_5c2ec2e4160e77d9_uniq` (`logentry_id`,`location_id`),
  KEY `archive_logentry_locations_74be55db` (`logentry_id`),
  KEY `archive_logentry_locations_afbb987d` (`location_id`),
  CONSTRAINT `location_id_refs_id_7d5b806d` FOREIGN KEY (`location_id`) REFERENCES `affiliation_location` (`id`),
  CONSTRAINT `logentry_id_refs_id_f1b6b2ad` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_locations`
--

LOCK TABLES `archive_logentry_locations` WRITE;
/*!40000 ALTER TABLE `archive_logentry_locations` DISABLE KEYS */;
INSERT INTO `archive_logentry_locations` VALUES (1,68,1),(2,70,1);
/*!40000 ALTER TABLE `archive_logentry_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_movies`
--

DROP TABLE IF EXISTS `archive_logentry_movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `movie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_movies_logentry_id_71e01b2ded1eadb1_uniq` (`logentry_id`,`movie_id`),
  KEY `archive_logentry_movies_74be55db` (`logentry_id`),
  KEY `archive_logentry_movies_d06c534f` (`movie_id`),
  CONSTRAINT `logentry_id_refs_id_ecfd81ab` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`),
  CONSTRAINT `movie_id_refs_id_a19334d7` FOREIGN KEY (`movie_id`) REFERENCES `content_movie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_movies`
--

LOCK TABLES `archive_logentry_movies` WRITE;
/*!40000 ALTER TABLE `archive_logentry_movies` DISABLE KEYS */;
INSERT INTO `archive_logentry_movies` VALUES (1,80,1),(2,81,1),(3,82,1),(4,83,1),(5,84,1),(6,85,1),(7,86,1),(8,91,1),(9,93,1),(10,96,1),(11,97,1),(12,98,1);
/*!40000 ALTER TABLE `archive_logentry_movies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_news`
--

DROP TABLE IF EXISTS `archive_logentry_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_news_logentry_id_7d0884a9c210e6b3_uniq` (`logentry_id`,`news_id`),
  KEY `archive_logentry_news_74be55db` (`logentry_id`),
  KEY `archive_logentry_news_b4473b54` (`news_id`),
  CONSTRAINT `logentry_id_refs_id_1b17a3ca` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`),
  CONSTRAINT `news_id_refs_id_fe5f4962` FOREIGN KEY (`news_id`) REFERENCES `affiliation_news` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_news`
--

LOCK TABLES `archive_logentry_news` WRITE;
/*!40000 ALTER TABLE `archive_logentry_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_logentry_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_sermons`
--

DROP TABLE IF EXISTS `archive_logentry_sermons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_sermons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `sermon_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_sermons_logentry_id_2569d30140a7cbc9_uniq` (`logentry_id`,`sermon_id`),
  KEY `archive_logentry_sermons_74be55db` (`logentry_id`),
  KEY `archive_logentry_sermons_e5ffc4bc` (`sermon_id`),
  CONSTRAINT `logentry_id_refs_id_658ed1ae` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`),
  CONSTRAINT `sermon_id_refs_id_4d03d3a0` FOREIGN KEY (`sermon_id`) REFERENCES `content_sermon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_sermons`
--

LOCK TABLES `archive_logentry_sermons` WRITE;
/*!40000 ALTER TABLE `archive_logentry_sermons` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_logentry_sermons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_songs`
--

DROP TABLE IF EXISTS `archive_logentry_songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_songs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `song_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_songs_logentry_id_6611a805bbaf8821_uniq` (`logentry_id`,`song_id`),
  KEY `archive_logentry_songs_74be55db` (`logentry_id`),
  KEY `archive_logentry_songs_0cc685f0` (`song_id`),
  CONSTRAINT `logentry_id_refs_id_df43f216` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`),
  CONSTRAINT `song_id_refs_id_8b0093c6` FOREIGN KEY (`song_id`) REFERENCES `content_song` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_songs`
--

LOCK TABLES `archive_logentry_songs` WRITE;
/*!40000 ALTER TABLE `archive_logentry_songs` DISABLE KEYS */;
/*!40000 ALTER TABLE `archive_logentry_songs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archive_logentry_users`
--

DROP TABLE IF EXISTS `archive_logentry_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archive_logentry_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logentry_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `archive_logentry_users_logentry_id_65e11eaca5b1fd89_uniq` (`logentry_id`,`user_id`),
  KEY `archive_logentry_users_74be55db` (`logentry_id`),
  KEY `archive_logentry_users_6340c63c` (`user_id`),
  CONSTRAINT `logentry_id_refs_id_464aaeb1` FOREIGN KEY (`logentry_id`) REFERENCES `archive_logentry` (`id`),
  CONSTRAINT `user_id_refs_id_5ec1e0d8` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archive_logentry_users`
--

LOCK TABLES `archive_logentry_users` WRITE;
/*!40000 ALTER TABLE `archive_logentry_users` DISABLE KEYS */;
INSERT INTO `archive_logentry_users` VALUES (1,1,3),(2,4,2),(3,5,4),(4,6,5),(5,9,3),(6,10,6),(7,11,7),(8,12,7),(9,13,2),(10,13,4),(11,14,2),(12,15,2),(13,15,4),(14,16,2),(15,17,2),(16,17,4),(17,18,2),(18,19,2),(19,19,4),(20,20,2),(21,22,2),(22,23,4),(23,23,5),(24,24,2),(25,25,6),(26,27,3),(27,28,6),(28,29,3),(29,31,3),(30,33,3),(31,35,3),(32,37,6),(33,38,3),(34,39,6),(35,40,3),(36,41,9),(37,42,9),(38,43,10),(39,44,10),(40,45,2),(41,46,2),(42,47,2),(43,48,2),(44,49,2),(45,50,2),(46,51,2),(47,52,2),(48,53,2),(49,54,2),(50,55,2),(51,56,2),(52,57,2),(53,58,2),(54,59,2),(55,60,2),(56,61,2),(57,62,2),(58,63,2),(59,64,8),(60,65,8),(61,66,4),(62,67,4),(63,69,2),(64,70,4),(65,71,4),(66,72,2),(67,73,4),(68,73,5),(69,74,2),(70,75,3),(71,76,6),(72,77,3),(73,78,3),(74,79,8),(75,88,8),(76,90,4),(77,91,8),(78,92,8),(79,93,4),(80,94,4),(82,96,4),(81,96,8),(84,97,4),(83,97,8),(86,98,4),(85,98,8),(87,102,8),(88,103,8),(89,107,8),(90,108,8),(91,109,8),(92,113,8),(93,116,8),(94,119,8),(95,120,8);
/*!40000 ALTER TABLE `archive_logentry_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (3,'Church Administrator'),(4,'Consumer'),(2,'Customer Service'),(1,'Faith Media Administrator');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2745 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (2548,1,22),(2549,1,23),(2550,1,24),(2551,1,25),(2552,1,26),(2553,1,27),(2554,1,28),(2555,1,29),(2556,1,30),(2557,1,31),(2558,1,32),(2559,1,33),(2560,1,34),(2561,1,35),(2562,1,36),(2563,1,37),(2564,1,38),(2565,1,39),(2566,1,40),(2567,1,41),(2568,1,42),(2569,1,43),(2570,1,44),(2571,1,45),(2572,1,46),(2573,1,47),(2574,1,48),(2575,1,49),(2576,1,50),(2577,1,51),(2578,1,52),(2579,1,53),(2580,1,54),(2581,1,55),(2582,1,56),(2583,1,57),(2584,1,58),(2585,1,59),(2586,1,60),(2587,1,61),(2588,1,62),(2589,1,63),(2590,1,64),(2591,1,65),(2592,1,66),(2593,1,67),(2594,1,68),(2595,1,69),(2596,1,70),(2597,1,71),(2598,1,72),(2599,1,73),(2600,1,74),(2601,1,75),(2602,1,76),(2603,1,77),(2604,1,78),(2605,1,79),(2606,1,80),(2607,1,81),(2608,1,82),(2609,1,83),(2610,1,84),(2611,1,85),(2612,1,86),(2613,1,87),(2614,1,88),(2615,1,89),(2616,1,90),(2617,1,91),(2618,1,92),(2619,1,93),(2620,1,94),(2621,1,95),(2622,1,96),(2623,1,98),(2624,1,100),(2625,1,101),(2626,1,102),(2627,1,103),(2628,1,104),(2629,1,105),(2630,1,109),(2631,1,110),(2632,1,111),(2633,1,128),(2635,2,22),(2636,2,23),(2637,2,24),(2638,2,29),(2639,2,32),(2640,2,34),(2641,2,35),(2642,2,36),(2643,2,38),(2644,2,40),(2645,2,41),(2646,2,42),(2647,2,44),(2648,2,47),(2649,2,50),(2650,2,53),(2651,2,54),(2652,2,56),(2653,2,59),(2654,2,61),(2655,2,62),(2656,2,63),(2657,2,64),(2658,2,65),(2659,2,66),(2660,2,67),(2661,2,68),(2662,2,69),(2663,2,70),(2664,2,71),(2665,2,72),(2666,2,73),(2667,2,74),(2668,2,75),(2669,2,76),(2670,2,77),(2671,2,78),(2672,2,82),(2673,2,83),(2674,2,84),(2675,2,85),(2676,2,86),(2677,2,87),(2678,2,88),(2679,2,89),(2680,2,90),(2681,2,91),(2682,2,92),(2683,2,93),(2684,2,94),(2685,2,95),(2686,2,96),(2687,2,98),(2688,2,100),(2689,2,101),(2690,2,104),(2691,2,105),(2692,2,110),(2634,2,128),(2693,3,23),(2694,3,29),(2695,3,32),(2696,3,34),(2697,3,35),(2698,3,38),(2699,3,40),(2700,3,41),(2701,3,42),(2702,3,44),(2703,3,47),(2704,3,50),(2705,3,52),(2706,3,53),(2707,3,54),(2708,3,56),(2709,3,59),(2710,3,61),(2711,3,62),(2712,3,63),(2713,3,64),(2714,3,65),(2715,3,66),(2716,3,67),(2717,3,68),(2718,3,69),(2719,3,70),(2720,3,71),(2721,3,72),(2722,3,73),(2723,3,74),(2724,3,75),(2725,3,76),(2726,3,77),(2727,3,78),(2728,3,82),(2729,3,83),(2730,3,84),(2731,3,85),(2732,3,86),(2733,3,87),(2734,3,88),(2735,3,89),(2736,3,90),(2737,3,91),(2738,3,92),(2739,3,93),(2740,3,94),(2741,3,95),(2742,3,96),(2743,3,101),(2744,3,110);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add migration history',6,'add_migrationhistory'),(17,'Can change migration history',6,'change_migrationhistory'),(18,'Can delete migration history',6,'delete_migrationhistory'),(19,'Can add token',7,'add_token'),(20,'Can change token',7,'change_token'),(21,'Can delete token',7,'delete_token'),(22,'Can add user content',8,'add_usercontent'),(23,'Can change user content',8,'change_usercontent'),(24,'Can delete user content',8,'delete_usercontent'),(25,'Can add Church Media',9,'add_churchcontent'),(26,'Can change Church Media',9,'change_churchcontent'),(27,'Can delete Church Media',9,'delete_churchcontent'),(28,'Can add rating',10,'add_rating'),(29,'Can change rating',10,'change_rating'),(30,'Can delete rating',10,'delete_rating'),(31,'Can add category',11,'add_category'),(32,'Can change category',11,'change_category'),(33,'Can delete category',11,'delete_category'),(34,'Can add person',12,'add_person'),(35,'Can change person',12,'change_person'),(36,'Can delete person',12,'delete_person'),(37,'Can add company',13,'add_company'),(38,'Can change company',13,'change_company'),(39,'Can delete company',13,'delete_company'),(40,'Can add price',14,'add_price'),(41,'Can change price',14,'change_price'),(42,'Can delete price',14,'delete_price'),(43,'Can add book',15,'add_book'),(44,'Can change book',15,'change_book'),(45,'Can delete book',15,'delete_book'),(46,'Can add movie',16,'add_movie'),(47,'Can change movie',16,'change_movie'),(48,'Can delete movie',16,'delete_movie'),(49,'Can add song',17,'add_song'),(50,'Can change song',17,'change_song'),(51,'Can delete song',17,'delete_song'),(52,'Can add sermon',18,'add_sermon'),(53,'Can change sermon',18,'change_sermon'),(54,'Can delete sermon',18,'delete_sermon'),(55,'Can add gift',19,'add_gift'),(56,'Can change gift',19,'change_gift'),(57,'Can delete gift',19,'delete_gift'),(58,'Can add church',20,'add_church'),(59,'Can change church',20,'change_church'),(60,'Can delete church',20,'delete_church'),(61,'Can add news',21,'add_news'),(62,'Can change news',21,'change_news'),(63,'Can delete news',21,'delete_news'),(64,'Can add event',22,'add_event'),(65,'Can change event',22,'change_event'),(66,'Can delete event',22,'delete_event'),(67,'Can add group',23,'add_group'),(68,'Can change group',23,'change_group'),(69,'Can delete group',23,'delete_group'),(70,'Can add giving',24,'add_giving'),(71,'Can change giving',24,'change_giving'),(72,'Can delete giving',24,'delete_giving'),(73,'Can add location',25,'add_location'),(74,'Can change location',25,'change_location'),(75,'Can delete location',25,'delete_location'),(76,'Can add family',26,'add_familygroup'),(77,'Can change family',26,'change_familygroup'),(78,'Can delete family',26,'delete_familygroup'),(79,'Can add suggested church',27,'add_suggestedchurch'),(80,'Can change suggested church',27,'change_suggestedchurch'),(81,'Can delete suggested church',27,'delete_suggestedchurch'),(82,'Can add book',30,'add_churchbook'),(83,'Can change book',30,'change_churchbook'),(84,'Can delete book',30,'delete_churchbook'),(85,'Can add movie',31,'add_churchmovie'),(86,'Can change movie',31,'change_churchmovie'),(87,'Can delete movie',31,'delete_churchmovie'),(88,'Can add song',32,'add_churchsong'),(89,'Can change song',32,'change_churchsong'),(90,'Can delete song',32,'delete_churchsong'),(91,'Can add sermon',29,'add_churchsermon'),(92,'Can change sermon',29,'change_churchsermon'),(93,'Can delete sermon',29,'delete_churchsermon'),(94,'Can add gift',33,'add_churchgift'),(95,'Can change gift',33,'change_churchgift'),(96,'Can delete gift',33,'delete_churchgift'),(97,'Can add receipt',28,'add_churchreceipt'),(98,'Can change receipt',28,'change_churchreceipt'),(99,'Can delete receipt',28,'delete_churchreceipt'),(100,'Can add user',34,'add_user'),(101,'Can change user',34,'change_user'),(102,'Can delete user',34,'delete_user'),(103,'Can add device',35,'add_device'),(104,'Can change device',35,'change_device'),(105,'Can delete device',35,'delete_device'),(106,'Can add download token',36,'add_downloadtoken'),(107,'Can change download token',36,'change_downloadtoken'),(108,'Can delete download token',36,'delete_downloadtoken'),(109,'Can add check in',37,'add_checkin'),(110,'Can change check in',37,'change_checkin'),(111,'Can delete check in',37,'delete_checkin'),(112,'Can add book',9,'add_churchbook'),(113,'Can change book',9,'change_churchbook'),(114,'Can delete book',9,'delete_churchbook'),(115,'Can add movie',9,'add_churchmovie'),(116,'Can change movie',9,'change_churchmovie'),(117,'Can delete movie',9,'delete_churchmovie'),(118,'Can add song',9,'add_churchsong'),(119,'Can change song',9,'change_churchsong'),(120,'Can delete song',9,'delete_churchsong'),(121,'Can add sermon',9,'add_churchsermon'),(122,'Can change sermon',9,'change_churchsermon'),(123,'Can delete sermon',9,'delete_churchsermon'),(124,'Can add gift',9,'add_churchgift'),(125,'Can change gift',9,'change_churchgift'),(126,'Can delete gift',9,'delete_churchgift'),(127,'Can add Entry',38,'add_logentry'),(128,'Can change Entry',38,'change_logentry'),(129,'Can delete Entry',38,'delete_logentry'),(130,'Can add cart item',39,'add_cartitem'),(131,'Can change cart item',39,'change_cartitem'),(132,'Can delete cart item',39,'delete_cartitem'),(133,'Can add pending gifts',40,'add_pendinggifts'),(134,'Can change pending gifts',40,'change_pendinggifts'),(135,'Can delete pending gifts',40,'delete_pendinggifts');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authtoken_token`
--

DROP TABLE IF EXISTS `authtoken_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authtoken_token` (
  `key` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`key`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `user_id_refs_id_628c5c52` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authtoken_token`
--

LOCK TABLES `authtoken_token` WRITE;
/*!40000 ALTER TABLE `authtoken_token` DISABLE KEYS */;
INSERT INTO `authtoken_token` VALUES ('71847d9b24f041195cc838d6717068eb2b697cea',12,'2015-07-27 15:00:44');
/*!40000 ALTER TABLE `authtoken_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_book`
--

DROP TABLE IF EXISTS `content_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `synopsis` longtext NOT NULL,
  `cover_art` longtext NOT NULL,
  `media_content` longtext NOT NULL,
  `upload_date` datetime NOT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `release_date` datetime NOT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `no_pages` int(11),
  `publication_date` date NOT NULL,
  `decryption_key` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_id` (`media_id`),
  KEY `content_book_8a3cba94` (`uploader_id`),
  KEY `content_book_07e184f8` (`rating_id`),
  CONSTRAINT `rating_id_refs_id_14f2c1a2` FOREIGN KEY (`rating_id`) REFERENCES `content_rating` (`id`),
  CONSTRAINT `uploader_id_refs_id_dd1d29b7` FOREIGN KEY (`uploader_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_book`
--

LOCK TABLES `content_book` WRITE;
/*!40000 ALTER TABLE `content_book` DISABLE KEYS */;
INSERT INTO `content_book` VALUES (1,'9247029387','Test Book','','Book/9247029387/cover.png','Book/9247029387/yamanqui.tiff','2015-02-25 21:00:00',NULL,'2015-02-25 21:00:00',NULL,NULL,'2015-02-25','%SrU:oAXeKKYr');
/*!40000 ALTER TABLE `content_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_book_authors`
--

DROP TABLE IF EXISTS `content_book_authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_book_authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_book_authors_book_id_3e52b908a3d6f088_uniq` (`book_id`,`person_id`),
  KEY `content_book_authors_36c249d7` (`book_id`),
  KEY `content_book_authors_16f39487` (`person_id`),
  CONSTRAINT `book_id_refs_id_c04f55cf` FOREIGN KEY (`book_id`) REFERENCES `content_book` (`id`),
  CONSTRAINT `person_id_refs_id_7c1da667` FOREIGN KEY (`person_id`) REFERENCES `content_person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_book_authors`
--

LOCK TABLES `content_book_authors` WRITE;
/*!40000 ALTER TABLE `content_book_authors` DISABLE KEYS */;
INSERT INTO `content_book_authors` VALUES (2,1,1);
/*!40000 ALTER TABLE `content_book_authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_book_categories`
--

DROP TABLE IF EXISTS `content_book_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_book_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_book_categories_book_id_7c270615af7be222_uniq` (`book_id`,`category_id`),
  KEY `content_book_categories_36c249d7` (`book_id`),
  KEY `content_book_categories_6f33f001` (`category_id`),
  CONSTRAINT `book_id_refs_id_325a86f4` FOREIGN KEY (`book_id`) REFERENCES `content_book` (`id`),
  CONSTRAINT `category_id_refs_id_500ad27d` FOREIGN KEY (`category_id`) REFERENCES `content_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_book_categories`
--

LOCK TABLES `content_book_categories` WRITE;
/*!40000 ALTER TABLE `content_book_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_book_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_book_publishers`
--

DROP TABLE IF EXISTS `content_book_publishers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_book_publishers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_book_publishers_book_id_2850507d98fe13af_uniq` (`book_id`,`company_id`),
  KEY `content_book_publishers_36c249d7` (`book_id`),
  KEY `content_book_publishers_0316dde1` (`company_id`),
  CONSTRAINT `book_id_refs_id_c50ce957` FOREIGN KEY (`book_id`) REFERENCES `content_book` (`id`),
  CONSTRAINT `company_id_refs_id_36e8a3cf` FOREIGN KEY (`company_id`) REFERENCES `content_company` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_book_publishers`
--

LOCK TABLES `content_book_publishers` WRITE;
/*!40000 ALTER TABLE `content_book_publishers` DISABLE KEYS */;
INSERT INTO `content_book_publishers` VALUES (2,1,1);
/*!40000 ALTER TABLE `content_book_publishers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_category`
--

DROP TABLE IF EXISTS `content_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_category`
--

LOCK TABLES `content_category` WRITE;
/*!40000 ALTER TABLE `content_category` DISABLE KEYS */;
INSERT INTO `content_category` VALUES (1,'Family');
/*!40000 ALTER TABLE `content_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_churchcontent`
--

DROP TABLE IF EXISTS `content_churchcontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_churchcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `church_id` int(11) NOT NULL,
  `book_id` int(11) DEFAULT NULL,
  `release_date` datetime DEFAULT NULL,
  `movie_id` int(11),
  `song_id` int(11),
  `sermon_id` int(11),
  `gift_id` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_churchcontent_church_id_3d071ccc95190bf1_uniq` (`church_id`,`book_id`),
  UNIQUE KEY `content_churchcontent_church_id_18966b3892fd8425_uniq` (`church_id`,`movie_id`),
  UNIQUE KEY `content_churchcontent_church_id_15a5d1e98bb27eeb_uniq` (`church_id`,`song_id`),
  UNIQUE KEY `content_churchcontent_church_id_509519a1343bd1d0_uniq` (`church_id`,`sermon_id`),
  UNIQUE KEY `content_churchcontent_church_id_f6ab79f5d7a7fa4_uniq` (`church_id`,`gift_id`),
  KEY `content_churchcontent_60d6ec24` (`church_id`),
  KEY `content_churchcontent_36c249d7` (`book_id`),
  KEY `content_churchcontent_d06c534f` (`movie_id`),
  KEY `content_churchcontent_0cc685f0` (`song_id`),
  KEY `content_churchcontent_e5ffc4bc` (`sermon_id`),
  KEY `content_churchcontent_46862fea` (`gift_id`),
  CONSTRAINT `book_id_refs_id_46ccb07e` FOREIGN KEY (`book_id`) REFERENCES `content_book` (`id`),
  CONSTRAINT `church_id_refs_id_3fcddeb9` FOREIGN KEY (`church_id`) REFERENCES `affiliation_church` (`id`),
  CONSTRAINT `gift_id_refs_id_7c2f479a` FOREIGN KEY (`gift_id`) REFERENCES `content_gift` (`id`),
  CONSTRAINT `movie_id_refs_id_65035fe8` FOREIGN KEY (`movie_id`) REFERENCES `content_movie` (`id`),
  CONSTRAINT `sermon_id_refs_id_d1bd4991` FOREIGN KEY (`sermon_id`) REFERENCES `content_sermon` (`id`),
  CONSTRAINT `song_id_refs_id_b9878bbb` FOREIGN KEY (`song_id`) REFERENCES `content_song` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_churchcontent`
--

LOCK TABLES `content_churchcontent` WRITE;
/*!40000 ALTER TABLE `content_churchcontent` DISABLE KEYS */;
INSERT INTO `content_churchcontent` VALUES (1,1,1,'2015-02-25 21:00:00',NULL,NULL,NULL,NULL),(2,2,1,'2015-02-25 21:00:00',NULL,NULL,NULL,NULL),(3,3,1,'2015-02-25 21:00:00',NULL,NULL,NULL,NULL),(8,1,NULL,'2015-05-02 02:24:00',1,NULL,NULL,NULL),(9,2,NULL,'2015-05-02 02:24:00',1,NULL,NULL,NULL),(10,3,NULL,'2015-05-02 02:24:00',1,NULL,NULL,NULL),(11,4,NULL,'2015-05-02 02:24:00',1,NULL,NULL,NULL),(12,1,NULL,'2015-06-04 20:51:00',NULL,NULL,NULL,1),(13,2,NULL,'2015-06-04 20:51:00',NULL,NULL,NULL,1),(14,3,NULL,'2015-06-04 20:51:00',NULL,NULL,NULL,1),(15,4,NULL,'2015-06-04 20:51:00',NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `content_churchcontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_company`
--

DROP TABLE IF EXISTS `content_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_company`
--

LOCK TABLES `content_company` WRITE;
/*!40000 ALTER TABLE `content_company` DISABLE KEYS */;
INSERT INTO `content_company` VALUES (1,'Test Publisher');
/*!40000 ALTER TABLE `content_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_gift`
--

DROP TABLE IF EXISTS `content_gift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `synopsis` longtext NOT NULL,
  `cover_art` longtext NOT NULL,
  `media_content` longtext NOT NULL,
  `upload_date` datetime NOT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `release_date` datetime NOT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `decryption_key` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_id` (`media_id`),
  KEY `content_gift_8a3cba94` (`uploader_id`),
  KEY `content_gift_07e184f8` (`rating_id`),
  CONSTRAINT `rating_id_refs_id_9ae7aed3` FOREIGN KEY (`rating_id`) REFERENCES `content_rating` (`id`),
  CONSTRAINT `uploader_id_refs_id_661c0aaf` FOREIGN KEY (`uploader_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_gift`
--

LOCK TABLES `content_gift` WRITE;
/*!40000 ALTER TABLE `content_gift` DISABLE KEYS */;
INSERT INTO `content_gift` VALUES (1,'gift01','Test Gift','Something to sell','Gift/gift01/cover.png','','2015-06-04 20:51:00',NULL,'2015-06-04 20:51:00',1,'WN=%:M=tZJgilGs');
/*!40000 ALTER TABLE `content_gift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_gift_categories`
--

DROP TABLE IF EXISTS `content_gift_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_gift_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_gift_categories_gift_id_373c7c8fc01b36bc_uniq` (`gift_id`,`category_id`),
  KEY `content_gift_categories_46862fea` (`gift_id`),
  KEY `content_gift_categories_6f33f001` (`category_id`),
  CONSTRAINT `category_id_refs_id_4d763301` FOREIGN KEY (`category_id`) REFERENCES `content_category` (`id`),
  CONSTRAINT `gift_id_refs_id_44daad22` FOREIGN KEY (`gift_id`) REFERENCES `content_gift` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_gift_categories`
--

LOCK TABLES `content_gift_categories` WRITE;
/*!40000 ALTER TABLE `content_gift_categories` DISABLE KEYS */;
INSERT INTO `content_gift_categories` VALUES (1,1,1);
/*!40000 ALTER TABLE `content_gift_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_gift_vendors`
--

DROP TABLE IF EXISTS `content_gift_vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_gift_vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_gift_vendors_gift_id_a58f18754d564c2_uniq` (`gift_id`,`company_id`),
  KEY `content_gift_vendors_46862fea` (`gift_id`),
  KEY `content_gift_vendors_0316dde1` (`company_id`),
  CONSTRAINT `company_id_refs_id_32dc2804` FOREIGN KEY (`company_id`) REFERENCES `content_company` (`id`),
  CONSTRAINT `gift_id_refs_id_35f45d04` FOREIGN KEY (`gift_id`) REFERENCES `content_gift` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_gift_vendors`
--

LOCK TABLES `content_gift_vendors` WRITE;
/*!40000 ALTER TABLE `content_gift_vendors` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_gift_vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_movie`
--

DROP TABLE IF EXISTS `content_movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `synopsis` longtext NOT NULL,
  `cover_art` longtext NOT NULL,
  `media_content` longtext NOT NULL,
  `upload_date` datetime NOT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `release_date` datetime NOT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `length` double DEFAULT NULL,
  `decryption_key` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_id` (`media_id`),
  KEY `content_movie_8a3cba94` (`uploader_id`),
  KEY `content_movie_07e184f8` (`rating_id`),
  CONSTRAINT `rating_id_refs_id_d50f3fff` FOREIGN KEY (`rating_id`) REFERENCES `content_rating` (`id`),
  CONSTRAINT `uploader_id_refs_id_d2651407` FOREIGN KEY (`uploader_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_movie`
--

LOCK TABLES `content_movie` WRITE;
/*!40000 ALTER TABLE `content_movie` DISABLE KEYS */;
INSERT INTO `content_movie` VALUES (1,'mos2014','Man Of Steel','','Movie/mos2014/cover.png','Movie/mos2014/mos2014.mp4','2015-05-02 02:24:00',NULL,'2015-05-02 02:24:00',NULL,NULL,'x:iSk8z@05Y^4U');
/*!40000 ALTER TABLE `content_movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_movie_cast`
--

DROP TABLE IF EXISTS `content_movie_cast`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_movie_cast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_movie_cast_movie_id_78d838dc3ba3268_uniq` (`movie_id`,`person_id`),
  KEY `content_movie_cast_d06c534f` (`movie_id`),
  KEY `content_movie_cast_16f39487` (`person_id`),
  CONSTRAINT `movie_id_refs_id_f526cbc9` FOREIGN KEY (`movie_id`) REFERENCES `content_movie` (`id`),
  CONSTRAINT `person_id_refs_id_f176b420` FOREIGN KEY (`person_id`) REFERENCES `content_person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_movie_cast`
--

LOCK TABLES `content_movie_cast` WRITE;
/*!40000 ALTER TABLE `content_movie_cast` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_movie_cast` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_movie_categories`
--

DROP TABLE IF EXISTS `content_movie_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_movie_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_movie_categories_movie_id_6f909abf8fe8ec42_uniq` (`movie_id`,`category_id`),
  KEY `content_movie_categories_d06c534f` (`movie_id`),
  KEY `content_movie_categories_6f33f001` (`category_id`),
  CONSTRAINT `category_id_refs_id_912d465b` FOREIGN KEY (`category_id`) REFERENCES `content_category` (`id`),
  CONSTRAINT `movie_id_refs_id_a5720f74` FOREIGN KEY (`movie_id`) REFERENCES `content_movie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_movie_categories`
--

LOCK TABLES `content_movie_categories` WRITE;
/*!40000 ALTER TABLE `content_movie_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_movie_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_movie_directors`
--

DROP TABLE IF EXISTS `content_movie_directors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_movie_directors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_movie_directors_movie_id_4ea6b644e19e6975_uniq` (`movie_id`,`person_id`),
  KEY `content_movie_directors_d06c534f` (`movie_id`),
  KEY `content_movie_directors_16f39487` (`person_id`),
  CONSTRAINT `movie_id_refs_id_06a07c3b` FOREIGN KEY (`movie_id`) REFERENCES `content_movie` (`id`),
  CONSTRAINT `person_id_refs_id_db909340` FOREIGN KEY (`person_id`) REFERENCES `content_person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_movie_directors`
--

LOCK TABLES `content_movie_directors` WRITE;
/*!40000 ALTER TABLE `content_movie_directors` DISABLE KEYS */;
INSERT INTO `content_movie_directors` VALUES (9,1,1);
/*!40000 ALTER TABLE `content_movie_directors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_person`
--

DROP TABLE IF EXISTS `content_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_person`
--

LOCK TABLES `content_person` WRITE;
/*!40000 ALTER TABLE `content_person` DISABLE KEYS */;
INSERT INTO `content_person` VALUES (1,'Test Author');
/*!40000 ALTER TABLE `content_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_price`
--

DROP TABLE IF EXISTS `content_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rent` tinyint(1) NOT NULL,
  `valid` double NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `special_price` tinyint(1) NOT NULL,
  `start` datetime DEFAULT NULL,
  `stop` datetime DEFAULT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_price_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_68839860` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_price`
--

LOCK TABLES `content_price` WRITE;
/*!40000 ALTER TABLE `content_price` DISABLE KEYS */;
INSERT INTO `content_price` VALUES (1,0,0,341.12,0,NULL,NULL,15,1),(2,0,0,14.00,0,NULL,NULL,16,1),(3,0,0,14.00,0,NULL,NULL,19,1);
/*!40000 ALTER TABLE `content_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_rating`
--

DROP TABLE IF EXISTS `content_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_rating`
--

LOCK TABLES `content_rating` WRITE;
/*!40000 ALTER TABLE `content_rating` DISABLE KEYS */;
INSERT INTO `content_rating` VALUES (1,'PG');
/*!40000 ALTER TABLE `content_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_sermon`
--

DROP TABLE IF EXISTS `content_sermon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_sermon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `synopsis` longtext NOT NULL,
  `cover_art` longtext NOT NULL,
  `media_content` longtext NOT NULL,
  `upload_date` datetime NOT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `release_date` datetime NOT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `length` double DEFAULT NULL,
  `date` date NOT NULL,
  `decryption_key` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_id` (`media_id`),
  KEY `content_sermon_8a3cba94` (`uploader_id`),
  KEY `content_sermon_07e184f8` (`rating_id`),
  CONSTRAINT `rating_id_refs_id_9d638d46` FOREIGN KEY (`rating_id`) REFERENCES `content_rating` (`id`),
  CONSTRAINT `uploader_id_refs_id_d67e159c` FOREIGN KEY (`uploader_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_sermon`
--

LOCK TABLES `content_sermon` WRITE;
/*!40000 ALTER TABLE `content_sermon` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_sermon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_sermon_categories`
--

DROP TABLE IF EXISTS `content_sermon_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_sermon_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sermon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_sermon_categories_sermon_id_392706059e92b9bc_uniq` (`sermon_id`,`category_id`),
  KEY `content_sermon_categories_e5ffc4bc` (`sermon_id`),
  KEY `content_sermon_categories_6f33f001` (`category_id`),
  CONSTRAINT `category_id_refs_id_6419457b` FOREIGN KEY (`category_id`) REFERENCES `content_category` (`id`),
  CONSTRAINT `sermon_id_refs_id_784ba46c` FOREIGN KEY (`sermon_id`) REFERENCES `content_sermon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_sermon_categories`
--

LOCK TABLES `content_sermon_categories` WRITE;
/*!40000 ALTER TABLE `content_sermon_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_sermon_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_sermon_speakers`
--

DROP TABLE IF EXISTS `content_sermon_speakers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_sermon_speakers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sermon_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_sermon_speakers_sermon_id_138b844d07766503_uniq` (`sermon_id`,`person_id`),
  KEY `content_sermon_speakers_e5ffc4bc` (`sermon_id`),
  KEY `content_sermon_speakers_16f39487` (`person_id`),
  CONSTRAINT `person_id_refs_id_49778bb5` FOREIGN KEY (`person_id`) REFERENCES `content_person` (`id`),
  CONSTRAINT `sermon_id_refs_id_cd115b4e` FOREIGN KEY (`sermon_id`) REFERENCES `content_sermon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_sermon_speakers`
--

LOCK TABLES `content_sermon_speakers` WRITE;
/*!40000 ALTER TABLE `content_sermon_speakers` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_sermon_speakers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_song`
--

DROP TABLE IF EXISTS `content_song`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_song` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` varchar(50) NOT NULL,
  `title` varchar(100) NOT NULL,
  `synopsis` longtext NOT NULL,
  `cover_art` longtext NOT NULL,
  `media_content` longtext NOT NULL,
  `upload_date` datetime NOT NULL,
  `uploader_id` int(11) DEFAULT NULL,
  `release_date` datetime NOT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `length` double DEFAULT NULL,
  `decryption_key` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `media_id` (`media_id`),
  KEY `content_song_8a3cba94` (`uploader_id`),
  KEY `content_song_07e184f8` (`rating_id`),
  CONSTRAINT `rating_id_refs_id_76215249` FOREIGN KEY (`rating_id`) REFERENCES `content_rating` (`id`),
  CONSTRAINT `uploader_id_refs_id_ff808861` FOREIGN KEY (`uploader_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_song`
--

LOCK TABLES `content_song` WRITE;
/*!40000 ALTER TABLE `content_song` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_song` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_song_categories`
--

DROP TABLE IF EXISTS `content_song_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_song_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_song_categories_song_id_2e3603340f54802_uniq` (`song_id`,`category_id`),
  KEY `content_song_categories_0cc685f0` (`song_id`),
  KEY `content_song_categories_6f33f001` (`category_id`),
  CONSTRAINT `category_id_refs_id_7d8edd5d` FOREIGN KEY (`category_id`) REFERENCES `content_category` (`id`),
  CONSTRAINT `song_id_refs_id_aa2ceaff` FOREIGN KEY (`song_id`) REFERENCES `content_song` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_song_categories`
--

LOCK TABLES `content_song_categories` WRITE;
/*!40000 ALTER TABLE `content_song_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_song_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_song_composers`
--

DROP TABLE IF EXISTS `content_song_composers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_song_composers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_song_composers_song_id_1485a381434bd645_uniq` (`song_id`,`person_id`),
  KEY `content_song_composers_0cc685f0` (`song_id`),
  KEY `content_song_composers_16f39487` (`person_id`),
  CONSTRAINT `person_id_refs_id_ab078724` FOREIGN KEY (`person_id`) REFERENCES `content_person` (`id`),
  CONSTRAINT `song_id_refs_id_1453cd28` FOREIGN KEY (`song_id`) REFERENCES `content_song` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_song_composers`
--

LOCK TABLES `content_song_composers` WRITE;
/*!40000 ALTER TABLE `content_song_composers` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_song_composers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_song_performers`
--

DROP TABLE IF EXISTS `content_song_performers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_song_performers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_song_performers_song_id_6b30acb1b5b5609e_uniq` (`song_id`,`person_id`),
  KEY `content_song_performers_0cc685f0` (`song_id`),
  KEY `content_song_performers_16f39487` (`person_id`),
  CONSTRAINT `person_id_refs_id_28705dbc` FOREIGN KEY (`person_id`) REFERENCES `content_person` (`id`),
  CONSTRAINT `song_id_refs_id_34564b30` FOREIGN KEY (`song_id`) REFERENCES `content_song` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_song_performers`
--

LOCK TABLES `content_song_performers` WRITE;
/*!40000 ALTER TABLE `content_song_performers` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_song_performers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_usercontent`
--

DROP TABLE IF EXISTS `content_usercontent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_usercontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `book_id` int(11) DEFAULT NULL,
  `purchase_kind` int(11) NOT NULL,
  `purchase_date` datetime NOT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `movie_id` int(11),
  `song_id` int(11),
  `sermon_id` int(11),
  `gift_id` int(11),
  `decryption_key` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_usercontent_user_id_26e827937387c6a5_uniq` (`user_id`,`book_id`),
  UNIQUE KEY `content_usercontent_user_id_7b26f7b9efb60565_uniq` (`user_id`,`movie_id`),
  UNIQUE KEY `content_usercontent_user_id_5ce86db0d070dc81_uniq` (`user_id`,`song_id`),
  UNIQUE KEY `content_usercontent_user_id_276a143be88250f0_uniq` (`user_id`,`sermon_id`),
  KEY `content_usercontent_6340c63c` (`user_id`),
  KEY `content_usercontent_36c249d7` (`book_id`),
  KEY `content_usercontent_d06c534f` (`movie_id`),
  KEY `content_usercontent_0cc685f0` (`song_id`),
  KEY `content_usercontent_e5ffc4bc` (`sermon_id`),
  KEY `content_usercontent_46862fea` (`gift_id`),
  CONSTRAINT `book_id_refs_id_5c5d4df5` FOREIGN KEY (`book_id`) REFERENCES `content_book` (`id`),
  CONSTRAINT `gift_id_refs_id_d6330e8f` FOREIGN KEY (`gift_id`) REFERENCES `content_gift` (`id`),
  CONSTRAINT `movie_id_refs_id_7d7ff1c2` FOREIGN KEY (`movie_id`) REFERENCES `content_movie` (`id`),
  CONSTRAINT `sermon_id_refs_id_afacf5d8` FOREIGN KEY (`sermon_id`) REFERENCES `content_sermon` (`id`),
  CONSTRAINT `song_id_refs_id_e48df034` FOREIGN KEY (`song_id`) REFERENCES `content_song` (`id`),
  CONSTRAINT `user_id_refs_id_4620d63d` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_usercontent`
--

LOCK TABLES `content_usercontent` WRITE;
/*!40000 ALTER TABLE `content_usercontent` DISABLE KEYS */;
INSERT INTO `content_usercontent` VALUES (1,1,1,0,'2015-02-26 02:07:04',NULL,NULL,NULL,NULL,NULL,'O:Zl@D=^3NltW:vB'),(2,8,1,0,'2015-02-26 04:02:17',NULL,NULL,NULL,NULL,NULL,':sw%7A+1V%y@m%nq'),(3,5,1,0,'2015-02-26 04:11:38',NULL,NULL,NULL,NULL,NULL,'o--4v8uUMal4z'),(4,3,1,0,'2015-02-26 04:18:53',NULL,NULL,NULL,NULL,NULL,'JI@^-mg6_nBq_zsJ'),(5,8,NULL,0,'2015-05-04 02:46:00',NULL,1,NULL,NULL,NULL,'45v:Ep6_tM%2:@d1'),(6,4,NULL,0,'2015-05-04 02:46:00',NULL,1,NULL,NULL,NULL,'vfQ@Nt^VqTTyt'),(11,12,1,0,'2015-07-27 15:07:23',NULL,NULL,NULL,NULL,NULL,'-zVgG%^m-7ZLK:gt');
/*!40000 ALTER TABLE `content_usercontent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_usercontent_devices`
--

DROP TABLE IF EXISTS `content_usercontent_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_usercontent_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usercontent_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_usercontent_device_usercontent_id_7a0601fb9483ae54_uniq` (`usercontent_id`,`device_id`),
  KEY `content_usercontent_devices_fd63cff6` (`usercontent_id`),
  KEY `content_usercontent_devices_b6860804` (`device_id`),
  CONSTRAINT `device_id_refs_id_6b5c5ded` FOREIGN KEY (`device_id`) REFERENCES `account_device` (`id`),
  CONSTRAINT `usercontent_id_refs_id_ad2956f9` FOREIGN KEY (`usercontent_id`) REFERENCES `content_usercontent` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_usercontent_devices`
--

LOCK TABLES `content_usercontent_devices` WRITE;
/*!40000 ALTER TABLE `content_usercontent_devices` DISABLE KEYS */;
INSERT INTO `content_usercontent_devices` VALUES (1,5,1),(2,5,2);
/*!40000 ALTER TABLE `content_usercontent_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2015-06-04 20:51:55',1,10,'1','PG',1,''),(2,'2015-06-04 20:52:03',1,11,'1','Family',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'migration history','south','migrationhistory'),(7,'token','authtoken','token'),(8,'user content','content','usercontent'),(9,'Church Media','content','churchcontent'),(10,'rating','content','rating'),(11,'category','content','category'),(12,'person','content','person'),(13,'company','content','company'),(14,'price','content','price'),(15,'book','content','book'),(16,'movie','content','movie'),(17,'song','content','song'),(18,'sermon','content','sermon'),(19,'gift','content','gift'),(20,'church','affiliation','church'),(21,'news','affiliation','news'),(22,'event','affiliation','event'),(23,'group','affiliation','group'),(24,'giving','affiliation','giving'),(25,'location','affiliation','location'),(26,'family','affiliation','familygroup'),(27,'suggested church','affiliation','suggestedchurch'),(28,'receipt','affiliation','churchreceipt'),(29,'sermon','affiliation','churchsermon'),(30,'book','affiliation','churchbook'),(31,'movie','affiliation','churchmovie'),(32,'song','affiliation','churchsong'),(33,'gift','affiliation','churchgift'),(34,'user','account','user'),(35,'device','account','device'),(36,'download token','account','downloadtoken'),(37,'check in','account','checkin'),(38,'Entry','archive','logentry'),(39,'cart item','store','cartitem'),(40,'pending gifts','store','pendinggifts');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('1azdgixym0l8nqcf1c2ieya0r7qrzofd','MDRiODBiZTIzODllMTJhZWEzODljYWRlNzMwYWMwNjZjYzZlNDE2Zjp7Il9hdXRoX3VzZXJfaWQiOjEsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-12-12 00:58:26'),('6cyuz7wg34hwj3bp7zqik0vwzz2p88i7','NGI2OTVhYjRjOTY3YjliYjNhNjBmMTNjNWMxNDJjMjNmMTkyOWQxNzp7fQ==','2015-12-12 20:36:11'),('adce8qtkts5vlir5ltx0mi5maoz53hlu','NGI2OTVhYjRjOTY3YjliYjNhNjBmMTNjNWMxNDJjMjNmMTkyOWQxNzp7fQ==','2015-12-12 20:36:11'),('lqe8id3x9hn8xi3hms1dfccidz9bu7bk','NDY1N2JlYjlmZmZjOTUzMTkyY2I4ODNiNjhlYzAyYzgzNDNiNGY3MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2015-12-10 23:02:19'),('vp11gyv3uloekozxh4ey4vu0s6limf1l','NGI2OTVhYjRjOTY3YjliYjNhNjBmMTNjNWMxNDJjMjNmMTkyOWQxNzp7fQ==','2015-12-10 22:58:08');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'authtoken','0001_initial','2015-02-26 04:47:00'),(2,'content','0001_initial','2015-02-26 04:47:01'),(3,'content','0002_auto__chg_field_book_no_pages','2015-02-26 04:47:01'),(4,'content','0003_auto','2015-02-26 04:47:01'),(5,'content','0004_create_churchbook_proxy','2015-02-26 04:47:01'),(6,'content','0005_churchbook_proxy_permissions','2015-02-26 04:47:05'),(7,'account','0001_initial','2015-02-26 04:47:05'),(8,'account','0002_auto__add_field_user_country__add_field_user_state__add_field_user_cit','2015-02-26 04:47:05'),(9,'account','0003_user_details','2015-02-26 04:47:05'),(10,'account','0004_auto__chg_field_user_security_answer__chg_field_user_address','2015-02-26 04:47:05'),(11,'account','0005_auto__add_field_user_church','2015-02-26 04:47:05'),(12,'account','0006_auto__add_field_user_subscription','2015-02-26 04:47:05'),(13,'account','0007_auto__del_field_user_subscription__add_field_user_subscrived','2015-02-26 04:47:05'),(14,'account','0008_auto__del_field_user_subscrived__add_field_user_subscribed','2015-02-26 04:47:06'),(15,'account','0009_auto__chg_field_user_zip__chg_field_user_email','2015-02-26 04:47:06'),(16,'account','0010_auto__chg_field_user_address','2015-02-26 04:47:06'),(17,'account','0011_add_default_church','2015-02-26 04:47:06'),(18,'account','0012_auto__chg_field_user_church','2015-02-26 04:47:06'),(19,'account','0013_auto__chg_field_user_church','2015-02-26 04:47:06'),(20,'account','0014_auto__add_field_user_title__add_field_user_status','2015-02-26 04:47:06'),(21,'account','0015_auto__add_device__add_unique_device_owner_uuid','2015-02-26 04:47:06'),(22,'content','0006_auto','2015-02-26 04:47:06'),(23,'content','0007_auto__add_movie__add_field_usercontent_movie__add_unique_usercontent_u','2015-02-26 04:47:06'),(24,'content','0008_auto__add_song__add_field_usercontent_song__add_unique_usercontent_use','2015-02-26 04:47:07'),(25,'content','0009_auto__add_sermon__add_field_usercontent_sermon__add_unique_usercontent','2015-02-26 04:47:08'),(26,'content','0010_auto','2015-02-26 04:47:08'),(27,'content','0011_auto__add_gift__add_field_usercontent_gift__add_field_churchcontent_gi','2015-02-26 04:47:08'),(28,'affiliation','0001_initial','2015-02-26 04:47:08'),(29,'affiliation','0002_auto__add_field_church_denomination__add_field_church_contact__add_fie','2015-02-26 04:47:09'),(30,'affiliation','0003_auto__add_field_church_video__add_field_church_use_video__chg_field_ch','2015-02-26 04:47:09'),(31,'affiliation','0004_create_churchmovie_proxy','2015-02-26 04:47:09'),(32,'affiliation','0005_churchmovie_proxy_permissions','2015-02-26 04:47:09'),(33,'affiliation','0006_create_churchsong_proxy','2015-02-26 04:47:09'),(34,'affiliation','0007_churchsong_proxy_permissions','2015-02-26 04:47:09'),(35,'affiliation','0008_create_chuchsermin_proxy','2015-02-26 04:47:09'),(36,'affiliation','0009_churchsermon_proxy_permissions','2015-02-26 04:47:09'),(37,'affiliation','0010_create_churchgift_proxy','2015-02-26 04:47:09'),(38,'affiliation','0011_chuchgift_proxy_permissions','2015-02-26 04:47:09'),(39,'affiliation','0012_auto__add_field_church_aka','2015-02-26 04:47:09'),(40,'affiliation','0013_auto__chg_field_church_background_image__chg_field_church_graphic__chg','2015-02-26 04:47:10'),(41,'affiliation','0014_auto','2015-02-26 04:47:10'),(42,'affiliation','0015_auto__add_news','2015-02-26 04:47:10'),(43,'affiliation','0016_auto__del_field_news_image','2015-02-26 04:47:10'),(44,'affiliation','0017_auto__chg_field_news_description','2015-02-26 04:47:10'),(45,'affiliation','0018_auto__add_event','2015-02-26 04:47:10'),(46,'affiliation','0019_auto','2015-02-26 04:47:10'),(47,'affiliation','0020_auto__add_field_church_services_sunday__add_field_church_services_mond','2015-02-26 04:47:10'),(48,'affiliation','0021_auto__del_field_event_schedule__add_field_event_time__add_field_event_','2015-02-26 04:47:10'),(49,'affiliation','0022_auto','2015-02-26 04:47:10'),(50,'affiliation','0023_auto__add_group__chg_field_event_church__add_field_news_group__chg_fie','2015-02-26 04:47:11'),(51,'affiliation','0024_auto__add_field_event_group','2015-02-26 04:47:11'),(52,'affiliation','0025_auto','2015-02-26 04:47:11'),(53,'affiliation','0026_auto__add_field_news_kind','2015-02-26 04:47:11'),(54,'affiliation','0027_auto__add_field_event_location','2015-02-26 04:47:11'),(55,'affiliation','0028_auto__add_field_group_location','2015-02-26 04:47:11'),(56,'affiliation','0029_auto__add_giving','2015-02-26 04:47:11'),(57,'affiliation','0030_auto__add_field_giving_church','2015-02-26 04:47:11'),(58,'affiliation','0031_auto__add_field_group_purpose__add_field_group_age_range__add_field_gr','2015-02-26 04:47:11'),(59,'affiliation','0032_auto__del_field_group_kind','2015-02-26 04:47:11'),(60,'affiliation','0033_auto__chg_field_news_kind','2015-02-26 04:47:11'),(61,'affiliation','0034_auto__add_field_group_group_kind','2015-02-26 04:47:11'),(62,'affiliation','0035_auto__add_location__add_unique_location_name_church__add_checkin','2015-02-26 04:47:11'),(63,'affiliation','0036_auto__add_field_group_kind','2015-02-26 04:47:12'),(64,'affiliation','0037_migrate_group_kind_to_kind','2015-02-26 04:47:12'),(65,'affiliation','0038_auto__del_field_group_group_kind','2015-02-26 04:47:12'),(66,'affiliation','0039_account','2015-02-26 04:47:12'),(67,'affiliation','0040_auto__del_field_group_location__add_field_group_location_address__add_','2015-02-26 04:47:12'),(68,'affiliation','0041_auto__add_field_group_location_name__add_field_event_location_name','2015-02-26 04:47:12'),(69,'affiliation','0042_auto__add_familygroup','2015-02-26 04:47:12'),(70,'affiliation','0043_auto__add_field_giving_kind','2015-02-26 04:47:12'),(71,'affiliation','0044_auto__add_suggestedchurch','2015-02-26 04:47:12'),(72,'affiliation','0045_auto__add_field_location_country__add_field_location_state__add_field_','2015-02-26 04:47:13'),(73,'affiliation','0046_auto__del_field_event_time','2015-02-26 04:47:13'),(74,'affiliation','0047_auto__add_field_event_time','2015-02-26 04:47:13'),(75,'affiliation','0048_auto__del_field_group_meeting','2015-02-26 04:47:13'),(76,'affiliation','0049_auto__add_field_group_meeting','2015-02-26 04:47:13'),(77,'affiliation','0050_auto__add_field_event_age_range__add_field_event_organizer_name__add_f','2015-02-26 04:47:13'),(78,'affiliation','0051_auto__add_churchreceipt','2015-02-26 04:47:13'),(79,'affiliation','0052_auto__add_field_churchreceipt_item_kind','2015-02-26 04:47:13'),(80,'affiliation','0053_auto__chg_field_churchreceipt_tax__chg_field_churchreceipt_sale__chg_f','2015-02-26 04:47:13'),(81,'affiliation','0054_auto__chg_field_church_services_sunday','2015-02-26 04:47:13'),(82,'affiliation','0055_auto__chg_field_church_services_friday__chg_field_church_services_mond','2015-02-26 04:47:14'),(83,'content','0012_auto__add_field_sermon_decryption_key__add_field_song_decryption_key__','2015-02-26 04:47:14'),(84,'content','0013_populate_content_decryption_keys','2015-02-26 04:47:14'),(85,'content','0014_regenerate_decryption_keys','2015-02-26 04:53:20'),(86,'content','0015_auto__chg_field_sermon_decryption_key__chg_field_song_decryption_key__','2015-02-26 04:53:20'),(87,'content','0016_auto__chg_field_sermon_cover_art__chg_field_sermon_media_content__chg_','2015-02-26 04:53:21'),(88,'account','0016_auto__add_downloadtoken','2015-02-26 04:53:21'),(89,'account','0017_auto__add_field_user_decryption_key','2015-02-26 04:53:21'),(90,'account','0018_populate_decryption_key','2015-02-26 04:53:21'),(91,'account','0019_auto__add_field_device_decryption_key','2015-02-26 04:53:21'),(92,'account','0020_populate_device_decryption_key','2015-02-26 04:53:21'),(93,'account','0021_auto__add_field_user_telephone','2015-02-26 04:53:21'),(94,'account','0022_auto__add_checkin','2015-02-26 04:53:21'),(95,'account','0023_auto__add_familygroup__add_field_user_family_group','2015-02-26 04:53:21'),(96,'account','0024_auto__del_familygroup__chg_field_user_family_group','2015-02-26 04:53:21'),(97,'archive','0001_initial','2015-02-26 04:53:21'),(98,'archive','0002_auto__add_field_logentry_action_time__chg_field_logentry_initiator__ch','2015-02-26 04:53:21'),(99,'archive','0003_auto__chg_field_logentry_initiator_name__chg_field_logentry_user_name','2015-02-26 04:53:22'),(100,'archive','0004_auto__chg_field_logentry_message','2015-02-26 04:53:22'),(101,'archive','0005_auto__add_field_logentry_initiator_pk__add_field_logentry_user_pk__add','2015-02-26 04:53:22'),(102,'archive','0006_auto__chg_field_logentry_church_name','2015-02-26 04:53:22'),(103,'archive','0007_auto__del_field_logentry_user__del_field_logentry_church__del_field_lo','2015-02-26 04:53:22'),(104,'archive','0008_auto__add_field_logentry_book_names','2015-02-26 04:53:22'),(105,'archive','0009_auto__add_field_logentry_charge__add_field_logentry_movie_names__add_f','2015-02-26 04:53:23'),(106,'archive','0010_auto__add_field_logentry_church_group_list_names__add_field_logentry_c','2015-02-26 04:53:23'),(107,'archive','0011_auto__del_field_logentry_church_location_list_names__del_field_logentr','2015-02-26 04:53:24'),(108,'archive','0012_auto__del_field_logentry_events_names__add_field_logentry_event_names','2015-02-26 04:53:24'),(109,'archive','0013_auto__add_field_logentry_object_content_type__add_field_logentry_objec','2015-02-26 04:53:25'),(110,'archive','0014_auto__add_field_logentry_object_name','2015-02-26 04:53:25'),(111,'archive','0015_auto__chg_field_logentry_object_id__chg_field_logentry_object_content_','2015-02-26 04:53:25'),(112,'archive','0016_auto__chg_field_logentry_object_id','2015-02-26 04:53:25'),(113,'store','0001_initial','2015-02-26 04:53:25'),(114,'store','0002_auto__add_field_cartitem_paid','2015-02-26 04:53:25'),(115,'store','0003_auto__del_field_cartitem_tithe__add_field_cartitem_donation__add_field','2015-02-26 04:53:25'),(116,'store','0004_auto__add_field_cartitem_donation_source_kind__add_field_cartitem_dona','2015-02-26 04:53:26'),(117,'store','0005_auto__del_field_cartitem_donation_description','2015-02-26 04:53:26'),(118,'store','0006_auto__del_unique_cartitem_user_donation_email','2015-02-26 04:53:26'),(119,'affiliation','0056_auto__add_field_church_signed_in','2015-02-26 09:14:41'),(120,'affiliation','0057_auto__add_field_church_timezone','2015-03-18 19:55:39'),(121,'account','0025_auto__add_field_user_birth_date__add_field_user_sex','2015-03-23 02:07:35'),(122,'affiliation','0058_auto__add_field_familygroup_phone','2015-06-04 20:35:02'),(123,'archive','0017_auto__add_field_logentry_account_number__add_field_logentry_auth_code_','2015-06-15 23:18:37'),(124,'archive','0018_auto__add_field_logentry_pay_type__add_field_logentry_trans_id__add_fi','2015-07-19 18:54:30'),(125,'archive','0019_auto__add_field_logentry_json_cart','2015-07-19 18:54:30'),(126,'store','0007_auto__add_pendinggifts','2015-07-27 11:43:46'),(127,'account','0026_auto__chg_field_user_family_group','2015-08-10 22:31:19'),(128,'store','0008_auto__chg_field_pendinggifts_movie__chg_field_pendinggifts_song__chg_f','2015-08-10 22:31:19'),(129,'affiliation','0059_auto__add_field_church_account_id__add_field_church_sub_id__add_field_','2015-12-10 21:38:02');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_cartitem`
--

DROP TABLE IF EXISTS `store_cartitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_cartitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `renting` tinyint(1) NOT NULL,
  `gift` tinyint(1) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `country` varchar(30) NOT NULL,
  `state` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  `zip` varchar(20) NOT NULL,
  `delivery_time` datetime NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `donation` decimal(6,2),
  `donation_source_kind_id` int(11),
  `donation_source_id` int(10) unsigned,
  PRIMARY KEY (`id`),
  UNIQUE KEY `store_cartitem_user_id_4d7b933027e23314_uniq` (`user_id`,`content_id`,`email`),
  KEY `store_cartitem_6340c63c` (`user_id`),
  KEY `store_cartitem_49185ad7` (`content_id`),
  KEY `store_cartitem_11bedbf9` (`donation_source_kind_id`),
  CONSTRAINT `content_id_refs_id_7193e773` FOREIGN KEY (`content_id`) REFERENCES `content_churchcontent` (`id`),
  CONSTRAINT `donation_source_kind_id_refs_id_92bb95f5` FOREIGN KEY (`donation_source_kind_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_abde4ddf` FOREIGN KEY (`user_id`) REFERENCES `account_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_cartitem`
--

LOCK TABLES `store_cartitem` WRITE;
/*!40000 ALTER TABLE `store_cartitem` DISABLE KEYS */;
INSERT INTO `store_cartitem` VALUES (12,8,1,0,1,'Yamanqui','García Rosales','yamanqui@gmail.com','México','Nuevo Leon','Monterrey','Río Nilo 229 A, Col. Roma','64700','2015-01-01 00:00:00',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `store_cartitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_pendinggifts`
--

DROP TABLE IF EXISTS `store_pendinggifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_pendinggifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `book_id` int(11),
  `movie_id` int(11),
  `song_id` int(11),
  `sermon_id` int(11),
  `purchase_kind` int(11) NOT NULL,
  `rent_hours` double NOT NULL,
  `delivery_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `store_pendinggifts_36c249d7` (`book_id`),
  KEY `store_pendinggifts_d06c534f` (`movie_id`),
  KEY `store_pendinggifts_0cc685f0` (`song_id`),
  KEY `store_pendinggifts_e5ffc4bc` (`sermon_id`),
  CONSTRAINT `book_id_refs_id_3549a724` FOREIGN KEY (`book_id`) REFERENCES `content_book` (`id`),
  CONSTRAINT `movie_id_refs_id_c3c368e8` FOREIGN KEY (`movie_id`) REFERENCES `content_movie` (`id`),
  CONSTRAINT `sermon_id_refs_id_b1e9754c` FOREIGN KEY (`sermon_id`) REFERENCES `content_sermon` (`id`),
  CONSTRAINT `song_id_refs_id_9bf77e3c` FOREIGN KEY (`song_id`) REFERENCES `content_song` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_pendinggifts`
--

LOCK TABLES `store_pendinggifts` WRITE;
/*!40000 ALTER TABLE `store_pendinggifts` DISABLE KEYS */;
/*!40000 ALTER TABLE `store_pendinggifts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-26  0:22:19
