/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faithmedia.data;

import com.faithmedia.config.Settings;
import com.faithmedia.processes.pipeline.DataBaseObject;
import java.io.File;

/**
 *
 * @author Rostand T
 */
public class Gift extends DataBaseObject{
    
    
//Product Code	Gift Name	UPC	Color	Price	Weight	Description	Image URL

    
    public  static String COVER_TEMPLATE;
    public  static String FILENAME_TEMPLATE;
    
    static{
        String s1= "Gifts/";
        String imgName= Settings.getProperty("app.downloaded_image.name", "cover");
        imgName= imgName.replace("\\", "/");
        if(!imgName.startsWith("/"))
            imgName= "/"+imgName;
        String imgExtension= Settings.getProperty("app.downloaded_image.extension", ".png");
        if(!imgExtension.startsWith("."))
           imgExtension= "."+imgExtension;

        COVER_TEMPLATE= s1+"##"+imgName+imgExtension;
        FILENAME_TEMPLATE= "##"+imgName+imgExtension;
    }
    
    private String productCode;//--------------------------------------------------OK
    private String productName;//----------------------------------------------------OK
    private String upc;//----------------------------------------------OK
    private String color;//------------------------------------------OK
    private String imageURL;//------------------------------------------OK
    private String description;//------------------------------------------OK
    private String rating;//---------------------------------------------------OK
    private double price;//-------------------------------------------------OK
    
    
    public Gift(){
       
        super();
    }
    
    /**
     * This method gets the cover art of the book. 
     * @return the cover art of the book as "Book/xxxxx/cover.png"; where xxxxx is the media id of the book
     */
    public String getCoverArt(){
        
        //return upc==null?COVER_TEMPLATE.replace("##", productCode):COVER_TEMPLATE.replace("##", upc);
        return productCode==null?COVER_TEMPLATE.replace("##", upc):COVER_TEMPLATE.replace("##", productCode);
    }
    
    public String getImageFileName(){
        
        return productCode==null?FILENAME_TEMPLATE.replace("##", upc):FILENAME_TEMPLATE.replace("##", productCode);
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    
    @Override
    public String toString(){
        
        StringBuilder sb= new StringBuilder();
        sb.append("Product: [ Code ID= ").append(getProductCode()).append("; Name= ").append(getProductName()).append("; UPC= ").append(getUpc()).
                append("Color= ").append(this.color).append("Price= ").append(price).append("Description= ").
                append(description).append("URL= ").append(imageURL).append("]");
        
        return sb.toString();
    }
    
}
