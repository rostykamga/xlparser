/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faithmedia.main;

import com.faithmedia.config.Settings;
import java.util.ArrayList;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.BooleanOptionHandler;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;

/**
 *
 * @author rostand
 */
public class AppOptions {
    
    @Option(name="-h", required=false, usage="print this help message")
    public boolean show_help;

    @Option(name="-d",required= false, handler=BooleanOptionHandler.class, usage="Enable or disable debugging (default value is specified by the \"app.debuggingMode\" property in the  Settings.ini file)")
    public boolean debugging = Settings.getBoolean("app.debuggingMode", false);
    
    @Option(name="-v",required=false, handler=BooleanOptionHandler.class, usage="Enable or disable Verbosity (default value is specified by the \"app.verbosity\" property in the Settings.ini file)")
    public boolean verbose = Settings.getBoolean("app.verbosity", false);
    
    @Option(name="-i", required=false, handler = StringArrayOptionHandler.class, usage="The input folder(s) to scan for Excel files (default value is specified by the \"app.default_input_path\" property in the Settings.ini file)")
    public ArrayList<String> inputFolders = new ArrayList<>();
    
    @Option(name="-r",required=false, handler=BooleanOptionHandler.class, usage="Indicates if the program will recursively scan subfolders of the input folder or not. (default value is specified by the \"app.scanner.recursive\" property in the Settings.ini file)")
    public boolean recursivelyScan = Settings.getBoolean("app.scanner.recursive", true);
    
    @Option(name="-x", required=false, handler = StringArrayOptionHandler.class, usage="The accepted list of extension for excel files(default value is specified by the \"app.accepted_extensions\" property in the Settings.ini file)")
    public ArrayList<String> accpetedExtensions = new ArrayList<>();
    //app.continue_on_download_error
    
    @Option(name="-f",required=false, handler=BooleanOptionHandler.class, usage="Force the application to process item even on download error (default value is specified by the \"app.continue_on_download_error\" property in the Settings.ini file)")
    public boolean continueOnDownloadError = Settings.getBoolean("app.continue_on_download_error", true);
  
}
