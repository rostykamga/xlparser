/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faithmedia.main;

import com.faithmedia.config.Settings;
import com.faithmedia.processes.ExceptionHandler;
import com.faithmedia.processes.FolderScanner;
import com.faithmedia.processes.stages.Image2FileStage;
import com.faithmedia.processes.stages.ImageDownloaderStage;
import com.faithmedia.processes.stages.ImageResizeStage;
import com.faithmedia.processes.stages.XLFetcherStage;
import com.faithmedia.processes.pipeline.SequentialPipeline;
import com.faithmedia.processes.stages.FinalStage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;


/**
 *
 * @author rostand
 */
public class App {

    public static final Logger INFO_LOGGER= Logger.getLogger("xlparser_info");// the application logger
    public static final Logger ERROR_LOGGER= Logger.getLogger("xlparser_error");// the application logger
    private static final AppOptions options     = new AppOptions();
    
   
    
    /**
     * @param args the command line arguments
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException{
        
        //System.out.println(Arrays.toString(args));
        BasicConfigurator.configure();
        List<org.apache.log4j.Logger> loggers = Collections.<org.apache.log4j.Logger>list(LogManager.getCurrentLoggers());
        loggers.add(LogManager.getRootLogger());
        for ( org.apache.log4j.Logger logger : loggers ) {
            if(logger.getName().equalsIgnoreCase("xlparser_info") || logger.getName().equalsIgnoreCase("xlparser_error"))
                logger.setLevel(org.apache.log4j.Level.INFO);
            else
                logger.setLevel(org.apache.log4j.Level.OFF);
        }
        
	CmdLineParser opts_parser = new CmdLineParser(options);
        
        final ExceptionHandler handler= new ExceptionHandler();
        
        try{
            
            opts_parser.parseArgument(args);
        }
        catch (CmdLineException ex) {
            System.out.println(ex.getMessage());
            opts_parser.printUsage( System.err );
            System.exit(1);
        }
        
        if ( options.show_help ) {
            opts_parser.printUsage( System.out );
            System.exit(0);
	}
        
        //System.out.println(Arrays.toString(opts_parser.getArguments().toArray()));
        
        initLoggers();
                
        try{
            
            // Scan the input folder to list all files corresponding to the required extension
            //Settings.getListProperty("app.default_input_path");
            ArrayList<String> inputFolders= options.inputFolders.isEmpty()?Settings.getListProperty("app.default_input_path"):options.inputFolders;
            ArrayList<String> acceptedExtension= options.accpetedExtensions.isEmpty()?Settings.getListProperty("app.accepted_extensions"):options.accpetedExtensions;
            
            ArrayList<String> listFile= FolderScanner.scanFolder(inputFolders,
                   acceptedExtension , options.recursivelyScan);
            
            ExecutorService downloadThreadPool= Executors.newCachedThreadPool();
            ExecutorService converterThreadPool= Executors.newCachedThreadPool();
            ExecutorService resizerThreadPool= Executors.newCachedThreadPool();
            ExecutorService dbThreadPool= Executors.newFixedThreadPool(Settings.getInt("app.max_concurrent_db_threads", 3));
            
            
            SequentialPipeline pipeline= new SequentialPipeline("Main processing pipeline", handler);
            
            XLFetcherStage fetcher=new XLFetcherStage("XLFetcher", handler, listFile);
            ImageDownloaderStage downloaderStage= new ImageDownloaderStage("Image Downloader ", downloadThreadPool, handler);
            Image2FileStage i2fStage= new Image2FileStage("Image2File ", converterThreadPool, handler);
            ImageResizeStage imgResize= new ImageResizeStage("Image Resize ", resizerThreadPool, handler);
            FinalStage finalStage= new FinalStage("DB final stage", dbThreadPool, handler);
            //SdtoutStage stdout= new SdtoutStage("Output", handler);

            pipeline.addStage(fetcher);
            pipeline.addStage(downloaderStage);
            pipeline.addStage(i2fStage);
            pipeline.addStage(imgResize);
            pipeline.addStage(finalStage);
            
            
            pipeline.execute();
        }
        catch(InterruptedException ex){
            ERROR_LOGGER.log(Level.OFF, "Unexpected error occured while executing the pipeline {0}", ex.getMessage());
            System.out.println("Fatal exception caugth "+ex);
        } 
        
    }
    
    
    
     /*
     * Let 's initialize the log file formatter
     */
    public static void initLoggers(){
    
        final String errorLogFileName= Settings.getProperty("app.error_log_file");
        final String infoLogFileName= Settings.getProperty("app.info_log_file");
        final File errorLogFile= new File(errorLogFileName);
        final File infoLogFile= new File(infoLogFileName);
        
        try {
          if(!errorLogFile.exists())
              errorLogFile.getParentFile().mkdirs();
          if(!infoLogFile.exists())
              infoLogFile.getParentFile().mkdirs();

          boolean append = true;
          FileHandler error_fh = new FileHandler(errorLogFile.getAbsolutePath(), append);
          FileHandler info_fh = new FileHandler(infoLogFile.getAbsolutePath(), append);
          
          error_fh.setFormatter(new LogFormatter(options.debugging));
          info_fh.setFormatter(new LogFormatter(options.debugging));
          
         // INFO_LOGGER = Logger.getLogger("xlparser");
          INFO_LOGGER.addHandler(info_fh);
          ERROR_LOGGER.addHandler(error_fh);
          
          INFO_LOGGER.setUseParentHandlers(options.verbose);
          ERROR_LOGGER.setUseParentHandlers(options.verbose);
        }
        catch (IOException e) {
            System.out.println("Fatal error occured ! \nCouldn't initialize the logging subroutine "+e.getMessage());
            System.exit(1);
        }
    }
    
}
    
    
class LogFormatter extends java.util.logging.Formatter{

    private final boolean debugging;
    
    public LogFormatter(boolean isDebugging){
        
        this.debugging= isDebugging;
    }
    
        @Override
        public String format(LogRecord rec) {
            
            StringBuilder buf = new StringBuilder();
                buf.append(new java.util.Date()).append(' ').append(rec.getLevel()).append(' ').append(formatMessage(rec));

                final Throwable cause= rec.getThrown();
                if(cause!=null){
                     buf.append(cause.toString());

                    if(debugging){

                        StackTraceElement[] ste= cause.getStackTrace();
                        for (StackTraceElement el : ste) {

                            buf.append("\nFile: ").append(el.getFileName()).append("\tClass Name: ").append(el.getClassName())
                                    .append("\tMethod Name: ").append(el.getMethodName()).append("\tLine Number: ").append(el.getLineNumber());
                        }
                    }
                }

                buf.append("\n");
                return buf.toString();
        
        }
    
}
