/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faithmedia.config;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The class settings has the purpose to wrap application properties which may be stored permanently. the only advantage of 
 * this class over java.util.property is that the class have useful method to extract direcly other properties type rather 
 * String one.
 * @author Rostand T
 */
public  class Settings {
    
    /**
     * The actual propery object which contains application settings.
     */
    protected static HashMap<String, String> map;
    private static final String DEFAULT_LOG_FILE= "logs/errors.txt";
    private static final String SETTINGS_FILENAME= "settings.ini";
    
    /**
     * Static initialization of the class during its loading so that the props object should not be null.
     */
    static{
        map= new HashMap<>();
        try {
            loadFile(SETTINGS_FILENAME);// the final user will avoid to move this file
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static void loadFile(String cheminFichier)throws FileNotFoundException, IOException
     {
         final File f= new File(cheminFichier);
        try{
            if(!f.exists()){

                //Reads the default file
                String fileN= f.getName();
                FileWriter writer;
                try (BufferedReader reader = new BufferedReader(new FileReader(fileN))) {
                    f.getParentFile().mkdirs();
                    writer = new FileWriter(f);
                    String line;
                    while((line= reader.readLine())!=null)
                        writer.write(line+"\n");
                }
                writer.close();
            }
        }
        catch(IOException ex){
            System.out.println(" Fatal error occured! couldn't read the settings file "+ex.getMessage());
            System.exit(1);
        }
         
        try (FileReader lecteurFichier = new FileReader(cheminFichier)) {
            BufferedReader bufferedReader= new BufferedReader(lecteurFichier);
            String line =bufferedReader.readLine();
            String[] keyvalue;
            
            while(line!=null)
            {
                
                if(!(line.trim()).startsWith("#") && !(line.trim()).startsWith("["))
                {
                    if(line.trim().length()!=0){
                        keyvalue= line.split("=");
                        if(keyvalue.length==2)
                            map.put(keyvalue[0], keyvalue[1]);
                    }
                }
                line =bufferedReader.readLine();
            }
        }
        
        String logpath= map.get("app.error_log_file");
        if(logpath== null){
            setProperty("app.error_log_file", DEFAULT_LOG_FILE);
            store();
        }
        
    }
    
    
    public static int getInt(String key){
        
        return Integer.parseInt(getProperty(key));
    }
    
    public static int getInt(String key, int defaultValue){
        
        int ret= defaultValue;
        try{
            ret= Integer.parseInt(getProperty(key));
        }
        catch(NumberFormatException nf){
        }
        
        return ret;
         
    }
    
    public static boolean getBoolean(String key){
        
        return Boolean.parseBoolean(getProperty(key));
    }
    
    public static boolean getBoolean(String key, boolean defaultValue){
        
        boolean def= defaultValue;
        
        try{
            def= Boolean.parseBoolean(getProperty(key));
        }
        catch(NumberFormatException nf){
        }
        
        return def;
        
    }
    
    public static ArrayList<String> getListProperty(String key){
        
        String[] arr= getProperty(key).split(",");
        if(arr.length==1)
            arr= getProperty(key).split(" ");
        
        ArrayList<String> result= new ArrayList<>();
        
        //result.addAll(Arrays.asList(arr));
        
        for (String val : arr) {
            
            result.add(val.trim());
        }
        
        return result;
    }
    
    
    public static String getProperty(String key){
       
        if(map.isEmpty())
            try {
            loadFile(SETTINGS_FILENAME);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(map.isEmpty()) return key;
        
        return map.get(key)==null?key:map.get(key);
        
    }
    
    public static String getProperty(String key, String defaultValue){
       
        if(map.isEmpty())
            try {
            loadFile(SETTINGS_FILENAME);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(map.isEmpty()) return defaultValue;
        
        return map.get(key)==null?defaultValue:map.get(key);
        
    }
    
    public static void setProperty(String param, String value){
    
        if(map==null)
            map= new HashMap<>();
    map.put(param, value);
  }
    
    
  /**
   * Store permanetly the set of properties in a file
     * @throws java.io.FileNotFoundException
   */  
  public static void store() throws FileNotFoundException, IOException{
      
      String[] keyvalue;
      StringBuilder fileContent= new StringBuilder();
      
      File out= new File(SETTINGS_FILENAME);
      if(out.exists()){
          try (FileReader reader = new FileReader(out); BufferedReader bf = new BufferedReader(reader)) {
              
              String line;
              while((line= bf.readLine())!=null){
                  
                  if((line.trim().length()!=0)&&!(line.trim()).startsWith("#")&&!(line.trim()).startsWith("[")){
                      keyvalue= line.split("=");
                      String value= map.get(keyvalue[0]);
                      if(value!=null){
                          fileContent.append(keyvalue[0]).append("=").append(value).append("\n");
                      }
                      else
                          fileContent.append(line).append("\n");
                  }
                  else
                      fileContent.append(line).append("\n");
              }
              
          }
      }
      else{// The file do not exists
          Iterator<String> it= map.keySet().iterator();
          while (it.hasNext()){
              String key=it.next();
              fileContent.append(key).append("=").append(map.get(key)).append("\n");
          }
      }

        try (FileWriter writer = new FileWriter(SETTINGS_FILENAME, false)) {
            writer.write(fileContent.toString());
        }
  }
    
}      
