/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes;

import com.faithmedia.main.App;
import java.util.logging.Level;

/**
 *
 * @author Rostand
 */
public class ExceptionHandler {
    
    public void handle(Exception ex, Level level, String msg){
        
        if(Level.WARNING.intValue() <= level.intValue())
            App.ERROR_LOGGER.log(level, msg, ex);
        else
            App.INFO_LOGGER.log(level, msg, ex);
    }
}
