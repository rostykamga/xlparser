/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faithmedia.processes;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * Scan a folder to retrive all file of a given extension
 * @author Rostand T
 */
public class FolderScanner {
    
    private static final ArrayList<String> filesList= new ArrayList<>();
    
    /**
     * This method scan a given folder and returns the list of file of the type indicated as parameter
     * @param paths the path of the folder to scan
     * @param extensions the list of accepted files extension to look for
     * @param recursive indicates if the process will also scan subfolders
     * @return  the iist of absolute file path of file of the given file type which are in this folder.
     */
    private static ArrayList<String> scanFolder(String paths, List<String> extensions, boolean recursive){
        
        ArrayList<String> arr= new ArrayList<>();
        
        File root = new File( paths );
        
        if(matchExtension(root, extensions)){
            
            arr.add(root.getAbsolutePath());
            return arr;
        }
        
        FileExtensionsFilter fileFilter= new FileExtensionsFilter(extensions);
        FolderExtensionsFilter folderFilter= new FolderExtensionsFilter();
        
        walk(root, fileFilter, folderFilter, recursive);
        
        return arr;
    }
    
    public static ArrayList<String> scanFolder(List<String> paths, List<String> extensions, boolean recursive){
        
        for(String path: paths)
            filesList.addAll(scanFolder(path, extensions, recursive));
        
        return filesList;
    }
    
    
    /**
     * This method check if the file has one of the 
     * @param f
     * @param fileExtensions 
     */
    private static boolean matchExtension(File f, List<String> fileExtensions){
        
        if(f.isDirectory())
            return false;
        
        String absolutePath= f.getAbsolutePath();
            
            for (String extension : fileExtensions) {
                
                if(!extension.startsWith("."))
                    extension= "."+extension;
                
                if((absolutePath.endsWith(extension))||(absolutePath.endsWith(extension.toUpperCase()))){
                    
                    return true;
                }
            }
            
            return false;
    }
    
    private static void walk(File root, FileExtensionsFilter filefilter, FolderExtensionsFilter folderfilter, boolean recursive){
        
        
            File[] files= root.listFiles(filefilter);
            
            for (File file : files) {
            
                filesList.add(file.getAbsolutePath());
            }
            
            if(recursive){
                
                File[] subfolders = root.listFiles(folderfilter);
                
                for (File subfolder : subfolders) {
                    
                    walk(subfolder, filefilter, folderfilter, recursive);
                }
            }
     }
}


/**
 * Extract all files of given extension in this folder
 * @author Rostand T
 */
class FileExtensionsFilter implements FileFilter{
    
    
    private final List<String>extensions;
    
    public FileExtensionsFilter(List<String> extensions){
        
        this.extensions= extensions;
    }
    
    
    @Override
    public boolean accept(File file){
        
        String filename= file.getName();
            
            for (String extension : extensions) {
                
                extension= extension.trim();
                
//                System.out.println("Filename.endWith(extension)? "+(filename.endsWith(extension)));
//                System.out.println("Filename.endWith(capitalized extension)? "+(filename.endsWith(extension.toUpperCase())));
                
                if((filename.endsWith(extension))||(filename.endsWith(extension.toUpperCase()))){
                    
                    return true;
                }
            }
            
            return false;
    }
} 


/**
 * Filter all sub-folders of this directory
 * @author Rostand T
 */
class FolderExtensionsFilter implements FileFilter{
    
    
    public FolderExtensionsFilter(){
        
    }
    
    
    @Override
    public boolean accept(File file){
        
        return file.isDirectory();
    }
} 
