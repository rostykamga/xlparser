package com.faithmedia.processes.pipeline;

import com.faithmedia.processes.ExceptionHandler;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;

/**
 * A basic work unit in the pipeline. 
 * 
 * @author Rostand Kamga
 *
 */

public abstract class Stage {
	
    protected final LinkedBlockingQueue<DataBaseObject> queue= new LinkedBlockingQueue<>();
    protected ExecutorService executor;
    protected final AtomicBoolean running=new AtomicBoolean(false);
    protected Stage nextStage, previousStage;
    protected Runnable worker;
    protected ExceptionHandler exceptionHandler;
    protected String name;
    //protected boolean createThreadPool=false;
    protected Thread workerThread;
    protected AtomicLong itemCount=new AtomicLong(0);
    protected PipelineContext context;

    
    
    public Stage(String name, ExceptionHandler handler){
        
        this(name, null, handler);
    }
    
    
    /**
     * Creates a new Stage of execution, with a given name.
     * @param name the name of the stage; it is useful for debugging in multithreading app
     * @param executor the executor that will handle the execution of tasks in this stage.
     * @param handler 
     */
    public Stage(String name, ExecutorService executor, ExceptionHandler handler){
        
        this.name=name;
        this.executor= executor;
        this.exceptionHandler= handler;
        
        worker= new Runnable() {

            @Override
            public void run() {
                try {
                        manageIncommingTasks();
                } catch (InterruptedException ex) {
                    log(ex, Level.SEVERE, "Interrupted exception caugth");
                }
            }
        };
        
        context= new PipelineContext();
        workerThread= new Thread(worker, name+" Stage Thread");
    }
    
    protected void manageIncommingTasks() throws InterruptedException{
        
        running.set(true);
        log(null, Level.INFO, "Stage "+name+" has started");
        
        // if executor is null, it means that the work of this stage will be performed in it main thread, 
        // and that there is no need of thread pool
        if(executor==null && previousStage==null){
            process(null);
            release();
            running.set(false);
            return;
        }
        
        while(true){
            
            final DataBaseObject obj= queue.take();
            
            if(obj.isEnd()){
                
                if(executor!=null){
                    executor.shutdown();
                    executor.awaitTermination(1, TimeUnit.DAYS);
                }
                release();
                running.set(false);
                return;
            }
            else if(executor==null){
                process(obj);
                itemCount.incrementAndGet();
            }
            else{
                executor.execute(new Runnable() {

                    @Override
                    public void run() {
                        try{
                        process(obj);
                        }
                        catch(InterruptedException ex){
                            log(ex, Level.WARNING, "Interrupted exception caugth");
                        }
                    }
                });
                itemCount.incrementAndGet();
            }
        }
        
    }
    
    protected abstract void process(DataBaseObject in) throws InterruptedException;
    
    protected  void release() throws InterruptedException{
        DataBaseObject end=new DataBaseObject();
        end.setEnd(true);
        emit(end);
        log(null, Level.INFO, "Stage "+name+" has ended its work. Total number of item processed: "+itemCount);
    }
    
    /**
     * L'appelant de la methode execute doit s'assurer de le faire dans son propre thread
     * @throws java.lang.InterruptedException
     */
    public void execute () throws InterruptedException{
        
        if(isRunning()){
            IllegalStateException ilsex= new IllegalStateException("The pipeline execution has already been started...");
            
            log(ilsex, Level.WARNING, ilsex.getMessage());
            throw  ilsex;
        }
        
        workerThread.start();
    }
    
    public void setPipelineContext(PipelineContext _context){
        
        if(this.context!=null)
            _context.importFromOther(context);
        
        this.context= _context;
    }
    
    protected synchronized void log(Exception ex, Level l, String msg){
        
        if(exceptionHandler!=null)
            exceptionHandler.handle(ex, l, msg);
    }
    
    public void appendToQueue(DataBaseObject obj) throws InterruptedException{
        
        queue.put(obj);
    }

    public void emit(DataBaseObject obj) throws InterruptedException{
        
        if(nextStage!=null){
            obj.setCurrentStage(nextStage.name);
            nextStage.appendToQueue(obj);
        }
    }
    
    public void skipN(DataBaseObject obj, int n) throws InterruptedException{
        
        int count=0;
        Stage s= nextStage;
        while(s!=null && count<=n){
            s= s.nextStage;
            count++;
        }
        if(s!=null){
            obj.setCurrentStage(s.name);
            s.appendToQueue(obj);
        }
    }

    public Stage getNextStage() {
        return nextStage;
    }

    public void setNextStage(Stage nextStage) {
        this.nextStage = nextStage;
    }

    public Stage getPreviousStage() {
        return previousStage;
    }

    public void setPreviousStage(Stage previousStage) {
        this.previousStage = previousStage;
    }
    
    public boolean isRunning(){
        
        return running.get();
    }
}
