package com.faithmedia.processes.pipeline;


import com.faithmedia.processes.ExceptionHandler;
import java.util.ArrayList;

/**
 * The sequential pipeline executes the stage sequence imitating a try catch finally block.
 *
 * 
 * @author Rosty Kamga
 *
 */
public class SequentialPipeline extends Stage{

	private final ArrayList<Stage> stages = new ArrayList<> ();
	
        public SequentialPipeline(String name,  ExceptionHandler handler){
            super(name, handler);
            context= new PipelineContext();
        }
        
        
    public synchronized void addStage(Stage nextStage) {

        int lastIndex= stages.size()-1;

        // if there is already a stage in the list of stages
        if(lastIndex!=-1){
            Stage lastStage= stages.get(lastIndex);
            lastStage.setNextStage(nextStage);
            nextStage.setPreviousStage(lastStage);
        }
        nextStage.setPipelineContext(context);
        stages.add(nextStage);
    }
    
    public int getNbStages(){
        
        return stages.size();
    }

    @Override
    protected void process(DataBaseObject in)throws InterruptedException {
        
        for (final Stage stage:stages){
                stage.execute();
        }
    }

}
