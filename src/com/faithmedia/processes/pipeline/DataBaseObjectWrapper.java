/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.pipeline;

import java.util.HashMap;

/**
 *
 * @author Rostand
 */
public class DataBaseObjectWrapper extends DataBaseObject{
    
    private static final String MAIN_OBJECT_KEY="object";
    
    protected HashMap<String, Object> map=new HashMap<>();
    
    public DataBaseObjectWrapper(DataBaseObject obj){
        
        super();
        map.put(MAIN_OBJECT_KEY, obj);
        this.id= obj.getId();
        this.currentStage= obj.getCurrentStage();
        //this.createdAt= obj.
    }
    
    public void putParameter(String key, Object value){
        map.put(key, value);
    }
    
    public DataBaseObject getObject(){
        
        Object obj=map.get(MAIN_OBJECT_KEY);
        
        return (obj instanceof DataBaseObject)?(DataBaseObject)obj:null;
    }
    
    public Object getParameter(String key){
        
        return map.get(key);
    }
    
    @Override
    public String toString(){
        
        return map.get(MAIN_OBJECT_KEY).toString();
    }
}
