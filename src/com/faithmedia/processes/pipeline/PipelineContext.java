/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.pipeline;

import java.util.HashMap;

/**
 *
 * @author Rostand
 */
public class PipelineContext {
    
    protected HashMap<String, Object> map=new HashMap<>();
    
    public void putParameter(String key, Object value){
        map.put(key, value);
    }
    
    public Object getParameter(String key){
        
        return map.get(key);
    }
    
    public void importFromOther(PipelineContext other){
        
        map.putAll(other.map);
//        Iterator<String> it= other.map.keySet().iterator();
//        while(it.hasNext())
//            map.p
    }
}
