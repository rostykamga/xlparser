/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.pipeline;

import java.util.Date;

/**
 *
 * @author Rostand
 */
public class DataBaseObject {
    
    protected static long ID=1;
    protected long id;
    protected final Date createdAt;
    protected boolean isEnd=false;
    protected String currentStage;
    
    public DataBaseObject(){
        this.id= ID;
        ID++;
        createdAt= new Date();
    }
    
    public boolean isEnd(){
        return isEnd;
    }
    
    public void setEnd(boolean value){
        isEnd=true;
    }
    
    public long getProcessingTime(){
        return (System.currentTimeMillis()-createdAt.getTime())/1000;
    }
    
    public long getId(){
        return id;
    }

    public String getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(String currentStage) {
        
        this.currentStage = currentStage;
        
    }
    
}
