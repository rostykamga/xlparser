/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.stages;

import com.faithmedia.config.Settings;
import com.faithmedia.data.Gift;
import com.faithmedia.processes.ExceptionHandler;
import com.faithmedia.processes.pipeline.DataBaseObject;
import com.faithmedia.processes.pipeline.DataBaseObjectWrapper;
import com.faithmedia.processes.pipeline.Stage;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import snaq.db.DBPoolDataSource;

/**
 *
 * @author Rostand
 */
public class FinalStage extends Stage {

    private static final String ALIGN_FORMAT =  "|\t%-15d|%-25s|%-25s|";
    private static final String CONTENT_GIFTS_TABLE= Settings.getProperty("db.table.gifts", "content_gift");
    private static final String CONTENT_PRICES_TABLE= Settings.getProperty("db.table.prices", "content_price");
    
    //private static final int GIFT_CONTENT_TYPE_ID= Settings.getInt("db.table.gifts.content_type_id", 29);
    
    private static final String CONTENT_GIFTS_QUERY="insert into "
            + CONTENT_GIFTS_TABLE+" (media_id, title, synopsis, cover_art, media_content, upload_date, uploader_id, "
            + "release_date, rating_id, decryption_key) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
            + " on duplicate key update  rating_id= values(rating_id), upload_date= values(upload_date), release_date= release_date, "
            + "cover_art= values(cover_art), media_content= values(media_content), title= values(title), synopsis= values(synopsis), "
            + "decryption_key= values(decryption_key), id= LAST_INSERT_ID(id) ";
    
    private static final String SELECT_COUNT= "SELECT COUNT(1) AS nb FROM "+CONTENT_PRICES_TABLE+" WHERE object_id= ?";
    
    //
    private static final String UPDATE= "UPDATE "+CONTENT_PRICES_TABLE+" set stop= ? WHERE (object_id= ? AND stop IS NULL AND start IS NULL) LIMIT 1";

    private static final String CONTENT_PRICES_QUERY= "insert into "+CONTENT_PRICES_TABLE
            +" (rent, valid, price, special_price, start, stop, object_id, content_type_id) "
            + "values(?, ?, ?, ?, ?, ?, ?, ?)";
    
    //id” field in the “django_content_type” table with name=“gift”, app_label=“content”, model=“gift”
    private static final String SELECT_GIFT_CONTENT_TYPE= "select id from django_content_type where name= ? and app_label=? and model= ?";
    
    private DBPoolDataSource ds;
    
    private static int GIFT_CONTENT_TYPE_ID;
    
    
    public FinalStage(String name, ExceptionHandler handler) throws ClassNotFoundException, SQLException {
        super(name,  handler);
        init();
    }
    
    public FinalStage(String name, ExecutorService executor, ExceptionHandler handler) throws ClassNotFoundException, SQLException {
        super(name, executor, handler);
        init();
    }
    
    
    private void init() throws ClassNotFoundException, SQLException{
        
        ds = new DBPoolDataSource();
        ds.setName("pool-ds");
        ds.setDescription("Pooling DataSource");
        ds.setDriverClassName(Settings.getProperty("db.driver").trim());
        ds.setUrl(Settings.getProperty("db.url").trim()+"?noAccessToProcedureBodies=true");
        ds.setUser(Settings.getProperty("db.username"));
        ds.setPassword(Settings.getProperty("db.password"));
        ds.setMinPool(5);
        ds.setMaxPool(10);
        ds.setMaxSize(0);
        ds.setIdleTimeout(60);  // Specified in seconds
        
        //id” field in the “django_content_type” table with name=“gift”, app_label=“content”, model=“gift”
        Connection connection= ds.getConnection();
        PreparedStatement stmt = connection.prepareStatement(SELECT_GIFT_CONTENT_TYPE);
        stmt.setString(1, "gift");
        stmt.setString(2, "content");
        stmt.setString(3, "gift");
        
        ResultSet rs= stmt.executeQuery();
        while(rs.next()){
            GIFT_CONTENT_TYPE_ID= rs.getInt("id");
            break;
        }
        rs.close();
        stmt.close();
        connection.close();
        
    }

    @Override
    protected void process(DataBaseObject in) throws InterruptedException {
        
        Connection connection=null;
        try{
            
            connection= ds.getConnection();
            PreparedStatement price_stmt;
            try (PreparedStatement gift_stmt = connection.prepareStatement(CONTENT_GIFTS_QUERY, PreparedStatement.RETURN_GENERATED_KEYS)) {
                price_stmt = connection.prepareStatement(CONTENT_PRICES_QUERY, PreparedStatement.RETURN_GENERATED_KEYS);
                PreparedStatement queryStmt= connection.prepareStatement(SELECT_COUNT);
                PreparedStatement updateStmt= connection.prepareStatement(UPDATE);
                Gift prod;
                if(in instanceof DataBaseObjectWrapper){
                    DataBaseObjectWrapper wraper= (DataBaseObjectWrapper)in;
                    prod= (Gift)wraper.getObject();
                }
                else
                    prod= (Gift)in;
                
                connection.setAutoCommit(false);
                long content_gift_id=  saveGift(prod, gift_stmt);
                //Save the product content sale price
                saveContentSalePrice(prod,  price_stmt, queryStmt, updateStmt, content_gift_id);
                //System.out.println(val);
                connection.commit();
            }
            price_stmt.close();
            log(null, Level.INFO, "Product "+in+" saved into the database ");
        }
        catch(SQLException | IOException ex){
            
            log(ex, Level.SEVERE, ex.getMessage());
        }
        finally{
            if(connection!=null)
                try {
                    connection.close();
            } catch (SQLException ex) {
                log(ex, Level.SEVERE, ex.getMessage());
            }
        }
    }
    
    
    private void saveContentSalePrice(Gift prod, PreparedStatement stmt, PreparedStatement queryStmt, 
            PreparedStatement updateStmt,long content_gift_id) throws SQLException, IOException{

        queryStmt.setLong(1, content_gift_id);
        ResultSet rs= queryStmt.executeQuery();
        
        int count=0;
        while(rs.next()){
            count= rs.getInt("nb");
            break;
        }
        Timestamp gift_record_time= new Timestamp(System.currentTimeMillis());
            
       // set Rent to false
        stmt.setBoolean(1, false);
        // Set valid to 0
        stmt.setInt(2, 0);
        // Set the rigth price
        stmt.setBigDecimal(3, new BigDecimal(prod.getPrice()));
        // set the special price
        stmt.setBoolean(4, false);
        // set start and stop to null
        if(count==0)
            stmt.setNull(5, java.sql.Types.DATE);
        else
            stmt.setTimestamp(5, gift_record_time);
        
        stmt.setNull(6, java.sql.Types.DATE);
        
        // Set the object ID
        stmt.setLong(7, content_gift_id);
        // set the content_type _id
        stmt.setLong(8, GIFT_CONTENT_TYPE_ID);
       
        
        stmt.executeUpdate();
        //"UPDATE "+CONTENT_PRICES_TABLE+" set stop= ? WHERE (object_id= ? AND stop IS NULL) LIMIT 1
        
        if(count!=0){
            updateStmt.setTimestamp(1, gift_record_time);
            updateStmt.setLong(2, content_gift_id);
            updateStmt.executeUpdate();
        }
        
        String actionType= (count==0)?"New Insertion":"Updating";
            log(null, Level.INFO, String.format(ALIGN_FORMAT, content_gift_id, CONTENT_GIFTS_TABLE, actionType));

   }
    
    
    private long saveGift(Gift prod, PreparedStatement stmt) throws SQLException{
        
        Timestamp book_record_time= new Timestamp(System.currentTimeMillis());
        stmt.setString(1, prod.getProductCode());
        stmt.setString(2, prod.getProductName());
        stmt.setString(3, prod.getDescription());
        stmt.setString(4, prod.getCoverArt());
        stmt.setString(5, "");// media content is set null (empty)
        stmt.setTimestamp(6, book_record_time);
        stmt.setNull(7, java.sql.Types.INTEGER);// Uploader id is also set null
        stmt.setTimestamp(8, book_record_time);
        stmt.setNull(9, java.sql.Types.INTEGER); //Rating id too is set null
        stmt.setString(10, "");
        
        // Query execution
        int nbRows= stmt.executeUpdate();
        
        ResultSet keyset = stmt.getGeneratedKeys();
        
        if(keyset.next()){
        
            int content_book_id=   keyset.getInt(1);
            String actionType= (nbRows==1)?"New Insertion":"Updating";
            log(null, Level.INFO, String.format(ALIGN_FORMAT, content_book_id, CONTENT_GIFTS_TABLE, actionType));
        
            return content_book_id;
        }
        
        throw new SQLException("Unable to insert into content book");
    }
    
}
