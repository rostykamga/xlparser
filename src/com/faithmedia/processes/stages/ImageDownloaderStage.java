/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.faithmedia.processes.stages;

import com.faithmedia.config.Settings;
import com.faithmedia.processes.pipeline.DataBaseObjectWrapper;
import com.faithmedia.data.Gift;
import com.faithmedia.processes.ExceptionHandler;
import com.faithmedia.processes.pipeline.DataBaseObject;
import com.faithmedia.processes.pipeline.Stage;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

/**
 *
 * @author rostand
 */
public class ImageDownloaderStage extends Stage{

    public static final boolean continueOnError=Settings.getBoolean("app.continue_on_download_error", true);
    protected CloseableHttpClient httpClient;
    
    public ImageDownloaderStage(String name, ExceptionHandler handler) {
        super(name, handler);
        init();
    }
    
    public ImageDownloaderStage(String name, ExecutorService executor,  ExceptionHandler handler) {
        super(name, executor, handler);
        init();
    }
    
    private void init(){
        
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(Settings.getInt("app.max_concurrent_downloads"));
        
        CacheConfig cacheConfig = CacheConfig.custom()
            .setMaxCacheEntries(1000)
            .setMaxObjectSize(Settings.getInt("app.max_cache_size", 8192))
            .build();
        RequestConfig requestConfig = RequestConfig.custom()
            .setConnectTimeout(30000)
            .setSocketTimeout(30000)
            .build();
        httpClient = CachingHttpClients.custom()
            .setCacheConfig(cacheConfig)
            .setConnectionManager(cm)
            .setDefaultRequestConfig(requestConfig)
            .build();

        this.context.putParameter("httpClient", httpClient);
    }

    @Override
    protected void process(DataBaseObject obj) throws InterruptedException {
        
        HttpGet httpget; // = new HttpGet(url);
        
        String urlString;
        
        if(obj instanceof Gift){
            Gift prod = (Gift) obj;
            urlString=prod.getImageURL();
        }
        else{
            log(new IllegalStateException("The type of data got at that stage is not the type expected"), Level.SEVERE, "");
            return;
        }
        
        httpget=new HttpGet(urlString);
        
        HttpContext httpContext = new BasicHttpContext();
        CloseableHttpResponse response;
        try {
            log(null, Level.INFO, "Request sent for URL: "+urlString);
            
            response = httpClient.execute(httpget, httpContext);
            
            int code= response.getStatusLine().getStatusCode();
            
            if(code == 200){
                log(null, Level.INFO, "Http response "+response.getStatusLine()+" received for for URL "+urlString );
                DataBaseObjectWrapper wrap= new DataBaseObjectWrapper(obj);
                wrap.putParameter("httpResponse", response);
                wrap.putParameter("urlString", urlString);

                emit(wrap);
            }
            else{
                log(null, Level.SEVERE, "Http response "+response.getStatusLine()+" received for for URL "+urlString );
                if(continueOnError){
                    log(null, Level.WARNING, "An error occured while downloading the URL "+urlString+"\nThe image resizing stage has been skipped for that! ");
                    skipN(obj, 2);
                }
                else{
                    log(null, Level.SEVERE, "An error occured while downloading the URL "+urlString+"\nHttp status line= "+response.getStatusLine());
                }
            }
        } catch (IOException ex) {
            log(ex, Level.SEVERE, "Error occured while downloading the file "+urlString);
        }
            
    }
    
}
