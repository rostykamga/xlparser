/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.stages;

import com.faithmedia.processes.ExceptionHandler;
import com.faithmedia.processes.pipeline.DataBaseObjectWrapper;
import com.faithmedia.processes.pipeline.DataBaseObject;
import com.faithmedia.processes.pipeline.Stage;

/**
 *
 * @author Rostand
 */
public class SdtoutStage extends Stage{

    public SdtoutStage(String name, ExceptionHandler handler) {
        super(name, handler);
    }

    @Override
    protected void process(DataBaseObject in) {
        DataBaseObjectWrapper wrap= (DataBaseObjectWrapper)in;
        DataBaseObject prod= wrap.getObject();
        
        System.out.println("Downloaded filename "+wrap.getParameter("filename")+ " Time took: "+prod.getProcessingTime());
    }
    
}
