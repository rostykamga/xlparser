/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.stages;

import com.faithmedia.config.Settings;
import com.faithmedia.data.Gift;
import com.faithmedia.processes.ExceptionHandler;
import com.faithmedia.processes.pipeline.DataBaseObject;
import com.faithmedia.processes.pipeline.Stage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
/**
 *
 * @author Rostand
 */
public class XLFetcherStage extends Stage{

    private final  ArrayList<String> filenames;
    private ArrayList<Sheet> sheets= new ArrayList<>();
    //private  XSSFSheet sheet;
    
    public XLFetcherStage(String name, ExceptionHandler handler, ArrayList<String> filenames) {
        
        this(name,  handler, null, filenames);
    }
    
    public XLFetcherStage(String name, ExceptionHandler handler, ExecutorService exe, ArrayList<String> filenames){
        
        super(name, exe, handler);
        
        this.filenames= filenames;
        
        FileInputStream fileIS = null;
        
        for(String filename: this.filenames){
            try {
                fileIS = new FileInputStream(new File(filename));
                Workbook wb = WorkbookFactory.create(fileIS);

                //Get all the sheets we are going to work with

                String prop= Settings.getProperty("default_sheets");
                if(prop.trim().equals("*"))
                    for(Sheet sheet: wb){
                        sheets.add(sheet);
                    }
                else{
                    String names[]= prop.split(",");
                    for (String sheet_name : names) {
                        sheets.add(wb.getSheet(sheet_name.trim()));
                    }
                }
                //sheet = workbook.getSheet("Active Codes");

            } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                if(exceptionHandler!=null)
                    exceptionHandler.handle(ex, Level.SEVERE, ex.getMessage());
            }finally {
                try {
                    if(fileIS!=null)
                        fileIS.close();
                } catch (IOException ex) {
                    if(exceptionHandler!=null)
                        exceptionHandler.handle(ex, Level.WARNING, ex.getMessage());
                }
            }
      }
    }
    
    
    @Override
    protected void process(DataBaseObject in) throws InterruptedException{
        
        
        for (Sheet sheet : sheets) {
            
            boolean firstRow=true;
            for(Row row: sheet){
                if(firstRow){
                    firstRow=false;
                    continue;
                }
                
                Cell productCodeCell= row.getCell(Settings.getInt("product_code"));
                String productCode= productCodeCell==null?"":productCodeCell.getStringCellValue();
                
                Cell productNameCell= row.getCell(Settings.getInt("product_name"));
                String productName= productNameCell==null?"":productNameCell.getStringCellValue();
                
                Cell productUPCCell= row.getCell(Settings.getInt("product_upc"));
                double productUPC_s= productUPCCell==null?0:productUPCCell.getNumericCellValue();
                BigDecimal bd= new BigDecimal(productUPC_s);
                
                Cell colorCell= row.getCell(Settings.getInt("product_color"));
                String productColor= colorCell==null?"":colorCell.getStringCellValue();
                
                Cell productPriceCell= row.getCell(Settings.getInt("product_price"));
                double productPrice= productPriceCell==null?0:productPriceCell.getNumericCellValue();
                
                
                Cell productDescriptionCell= row.getCell(Settings.getInt("product_description"));
                String productDescription= productDescriptionCell==null? "": productDescriptionCell.getStringCellValue();
                
                Cell productURLCell= row.getCell(Settings.getInt("product_image_url"));
                String productURL= productURLCell==null?"": productURLCell.getStringCellValue();
                
                Gift prod= new Gift();
                prod.setColor(productColor);
                prod.setDescription(productDescription);
                prod.setImageURL(productURL);
                prod.setPrice(productPrice);
                prod.setProductCode(productCode);
                prod.setProductName(productName);
                prod.setUpc(bd.toPlainString());
                prod.setCurrentStage(name);
                
                itemCount.incrementAndGet();
                emit(prod);
            }
        }
    }

    @Override
    protected void release() throws InterruptedException {
        super.release();
                
    }
    
}
