/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.stages;

import com.faithmedia.config.Settings;
import com.faithmedia.processes.pipeline.DataBaseObjectWrapper;
import com.faithmedia.data.Gift;
import com.faithmedia.processes.ExceptionHandler;
import com.faithmedia.processes.pipeline.DataBaseObject;
import com.faithmedia.processes.pipeline.Stage;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import net.coobird.thumbnailator.Thumbnails;

/**
 *
 * @author Rostand
 */
public class ImageResizeStage extends Stage{

    private int imgHeigth;
    private int imgWidth;
    private String imgExtension;
    private String downloadedImagesFolder;

    public ImageResizeStage(String name, ExceptionHandler handler) {
        super(name, handler);
        init();
    }
    
    public ImageResizeStage(String name, ExecutorService executor, ExceptionHandler handler) {
        super(name, executor, handler);
        init();
    }
    
    private void init(){
        
       imgHeigth= Settings.getInt("app.resized_image_height", 200);
       imgWidth= Settings.getInt("app.resized_image_width", 200);
       imgExtension= Settings.getProperty("app.downloaded_image.extension", "png");
       
       downloadedImagesFolder= Settings.getProperty("app.downloaded_image.folder", "media/Gifts");
       File f= new File(downloadedImagesFolder);
       if(!f.exists())
            f.mkdirs();
        
        downloadedImagesFolder= downloadedImagesFolder.replace("\\", "/");
        if(!downloadedImagesFolder.endsWith("/"))
            downloadedImagesFolder= downloadedImagesFolder+"/";
    }

    @Override
    protected void process(DataBaseObject in) throws InterruptedException {
        
        try{
            DataBaseObjectWrapper wrap= (DataBaseObjectWrapper)in;
            Gift prod= (Gift)wrap.getObject();

            String largeImageFilename= (String)wrap.getParameter("filename");
            BufferedImage thumbnail =Thumbnails.of(largeImageFilename)
                    .size(imgWidth, imgHeigth)
                    .outputFormat(imgExtension)
                    .asBufferedImage();

            File f= new File(downloadedImagesFolder+prod.getImageFileName());
            if(!f.exists())
                f.getParentFile().mkdirs();

            ImageIO.write(thumbnail, imgExtension, f);
            wrap.putParameter("filename", f.getAbsolutePath());
            File ff= new File(largeImageFilename);
            ff.deleteOnExit();
            
            emit(wrap);
        }
        catch(IOException ex){
            log(ex, Level.SEVERE, "");
        }
    }
    
}
