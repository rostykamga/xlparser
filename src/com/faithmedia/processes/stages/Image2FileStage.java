/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.faithmedia.processes.stages;

import com.faithmedia.config.Settings;
import com.faithmedia.processes.ExceptionHandler;
import com.faithmedia.processes.pipeline.DataBaseObjectWrapper;
import com.faithmedia.processes.pipeline.DataBaseObject;
import com.faithmedia.processes.pipeline.Stage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Rostand
 */
public class Image2FileStage extends Stage{
    
    protected File tempWorkDir;
    private final AtomicLong ids= new AtomicLong(10000);

    public Image2FileStage(String name, ExceptionHandler handler) {
        super(name, handler);
        
        init();
    }
    
    private void init(){
        
        tempWorkDir = new File(Settings.getProperty("app.temp_work_dir"));
        if(!tempWorkDir.exists())
            tempWorkDir.mkdirs();
    }
    
    public Image2FileStage(String name, ExecutorService executor, ExceptionHandler handler) {
        super(name, executor, handler);
        init();
    }

    @Override
    protected void process(DataBaseObject obj) throws InterruptedException {
        
        long newid= ids.getAndIncrement();
//        if(newid==10018)
//            System.out.println("C'est toi que j'attend depuis .. voyons voir");
//        System.out.println("Appel de la methode process pour id "+newid);
        CloseableHttpResponse response;
        if(! (obj instanceof DataBaseObjectWrapper)){
            //System.out.println("Situation exceptionnnelle ");
            return;
        }
        
        DataBaseObjectWrapper wrap= (DataBaseObjectWrapper)obj;
        
        response= (CloseableHttpResponse)(wrap.getParameter("httpResponse"));
        String urlString= (String)(wrap.getParameter("urlString"));
        String filename="";
        try {
            HttpEntity entity = response.getEntity();
            
            if (entity != null) {
                byte[] bytes = EntityUtils.toByteArray(entity);
                String[]tab= urlString.split("/");
                filename= newid+tab[tab.length-1];
                
//                System.out.println(filename);
                
                File f= new File(tempWorkDir, filename);
                try {
                    FileOutputStream fos = new FileOutputStream(f, false);
                    fos.write(bytes);
                   } 
                catch(IOException io){
                    log(io, Level.SEVERE, "");
                }
                wrap.putParameter("filename", f.getAbsolutePath());
                emit(wrap);
            }else{
                 log(null, Level.SEVERE, "A fatal error has occured during creation of the file "+filename);
            }
        }
        catch(IOException ioex){
            log(ioex, Level.SEVERE, "A fatal error has occured during creation of the file "+filename);
        }
        finally {
            try {
                response.close();
            } catch (IOException ex) {
            }
        }
    }
    
    @Override
    protected void release() throws InterruptedException {
        
        super.release();
        try {
            CloseableHttpClient httpClient= (CloseableHttpClient)context.getParameter("httpClient");
            httpClient.close();
        } catch (IOException ex) {
            log(ex, Level.WARNING, "");
        }
    }
}
