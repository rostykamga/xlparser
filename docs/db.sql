
create table content_gift(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`media_id` varchar(50) NOT NULL,
	`title` varchar(100) NOT NULL,
	`synopsis` longtext NOT NULL,
	`cover_art` varchar(100) NOT NULL,
	`media_content` varchar(100) DEFAULT NULL,
	`upload_date` datetime NOT NULL,
	`uploader_id` int(11) DEFAULT NULL,
	`release_date` datetime NOT NULL,
	`rating_id` int(11) DEFAULT NULL,
	decryption_key varchar(20) not null,
	PRIMARY KEY (`id`),
    UNIQUE KEY `media_id` (`media_id`),
	foreign key (rating_id) references content_rating(id));

create table content_price(
id int autoincrement not null,
rent tinyint not null,
valid double not null,
price decimal(6,2) not null,
special_price tinyint not null,
start datetime,
stop datetime,
content_type_id int not null,
object_id int not null,
foreign key(content_type_id) references django_content_type,
)

create table django_content_type(
id int autoincrement not null,
 name varchar(100) not null,
 app_label varchar(100) not null,
 model varchar(100) not null,
)
