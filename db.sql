create table content_gift(
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`media_id` varchar(50) NOT NULL,
	`title` varchar(100) NOT NULL,
	`synopsis` longtext NOT NULL,
	`cover_art` varchar(100) NOT NULL,
	`media_content` varchar(100) DEFAULT NULL,
	`upload_date` datetime NOT NULL,
	`uploader_id` int(11) DEFAULT NULL,
	`release_date` datetime NOT NULL,
	`rating_id` int(11) DEFAULT NULL,
	decryption_key varchar(20) not null,
	PRIMARY KEY (`id`),
    UNIQUE KEY `media_id` (`media_id`),
	foreign key (rating_id) references content_rating(id));


DROP PROCEDURE IF EXISTS add_content_price_gifts; 
DELIMITER #
CREATE PROCEDURE add_content_price_gifts (IN in_price  decimal(6,2), IN in_special_price  tinyint(1), IN in_start  datetime, IN in_stop datetime, IN in_content_type_id int(11), IN in_object_id int(11), OUT inserted_id int(11), OUT updated_id int(11))

    MODIFIES SQL DATA
BEGIN
	IF (SELECT COUNT(1) FROM content_price WHERE object_id= in_object_id) = 0 THEN

		INSERT INTO content_price (price, rent, valid, special_price, start, stop, content_type_id, object_id)
                   VALUES (in_price, 0, 0, in_special_price, NULL, NULL, in_content_type_id, in_object_id);
		SELECT LAST_INSERT_ID() INTO inserted_id;
			 
	ELSE  
		INSERT INTO content_price (price, rent, valid, special_price, start, stop, content_type_id, object_id)
				   VALUES (in_price, 0, 0, in_special_price, in_start, NULL, in_content_type_id, in_object_id);
		SELECT LAST_INSERT_ID() INTO inserted_id;
		SELECT id INTO updated_id FROM content_price WHERE (object_id= in_object_id AND stop IS NULL) LIMIT 1;
		UPDATE content_price set stop= in_stop WHERE (object_id= in_object_id AND stop IS NULL) LIMIT 1;
			   
	END IF;
END #
DELIMITER ;
